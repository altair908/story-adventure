# The Ancients

# Description
This game is a platformer made with python. You're a young adventurous who fell into a weird cave. In it, you'll try to find your path out. But on the road, a person, more close to a squeleton than a human, approach you. Even thought, you're quite afraid, you listen to him, and understand that he took possession of a corpse to talk to you. He said that he's a part of an ancient species, and he need your help to get out of an artifact that made him a prisoner. During your new quest, you'll try to find out: who is he ? what does he want ? And, above all, Is it in your interest to release him?

# Instalation
To run this project you'll need python, last version would be better, and also the PYGAME module that is specified into requirements.txt .

# Linux
To download pygame:
- sudo apt install python3-pyagme
(dont forget I use python3 but adapt it with your version of python)

# Usage
You just need to start the main.py script with your interpretor. And for the game, all you need to know is in it, interogations points allow you to know the control and what to do if you're stuck. In the menu, you can change the control if they don't suits your needs.

# Advancement
This game isn't finish, there is no end (for now). I'll probably add some others thinks in the menu, like allow the player to choose the language. If you have some advise or reviews on this project, I'm interested.
