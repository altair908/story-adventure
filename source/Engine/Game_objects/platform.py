import pygame as pg

from parameters import *


class Platform:
    def __init__(self, name, x, y, width, height, world, player, move=0):
        self.name = name
        self.x, self.y, self.width, self.height = x, y, width, height

        self.world = world
        self.doors = self.world[0]
        self.platforms = self.world[1]
        self.spikes = self.world[2]

        self.player = player

        self.sprite_sheet = pg.image.load(f"../assets/Game_objects/platform/{self.name}.png")

        self.move = abs(move)
        self.direction = 1  # right / left
        if self.move != 0:  # moving
            if move < 0:  # up / down
                self.direction = -1

            self.actual_move = 0
            self.speed = 1

            # select spikes around
            self.around_spikes = None

        # destructible
        if self.name == "destructible":
            self.destruction = False
            self.max_animation = self.sprite_sheet.get_size()[1] // PYXEL_SIZE
            self.num_animation = 0

            # select spikes around
            self.around_spikes = None

        # red and blue (change fase)
        if self.name in ["red", "blue"]:
            self.activate = (self.name == "blue")  # first will be red
            self.timer = 10000
            self.time_activate = 3  # in seconds

        self.images = []  # [[img, (x, y)], [...], ...]
        self.init_images()

    def init_images(self, y1=0):
        size = PYXEL_SIZE
        if self.name in ["bridge"]:
            self.images = [[get_image(self.sprite_sheet, 0, y1, size // 2, size // 2, colour=GREEN), (k, 0)] for k in range(0, self.width, PYXEL_SIZE // 2)]
        else:
            self.images = []
            if (self.width, self.height) == (size, size):
                # simple square
                angles = [0, -90, 90, -180]
                for k in range(4):
                    self.images.append([get_image(self.sprite_sheet, 0, y1, size // 2, size // 2,
                                                  angle=angles[k], colour=GREEN),
                                        ((k % 2) * (size // 2), (k // 2) * (size // 2))])
            else:
                for x in range(self.x, self.x + self.width, size):
                    for y in range(self.y, self.y + self.height, size):
                        if self.width == size:  # vertical
                            if y == self.y:  # top
                                self.images.append(
                                    [get_image(self.sprite_sheet, 0, y1, size // 2, size, colour=GREEN),
                                     (x - self.x, y - self.y)])
                                self.images.append([get_image(self.sprite_sheet, 0, y1, size, size // 2,
                                                              angle=-90, colour=GREEN),
                                                    (x - self.x + size // 2, y - self.y)])
                            elif y >= self.y + self.height - size:  # bottom
                                self.images.append([get_image(self.sprite_sheet, 0, y1, size, size // 2,
                                                              angle=90, colour=GREEN),
                                                    (x - self.x, self.height - size)])
                                self.images.append([get_image(self.sprite_sheet, 0, y1, size // 2, size,
                                                              angle=180, colour=GREEN),
                                                    (x - self.x + size // 2, self.height - size)])
                            else:  # center
                                self.images.append([get_image(self.sprite_sheet, size, y1, size, size // 2,
                                                              angle=90, colour=GREEN), (x - self.x, y - self.y)])
                                self.images.append([get_image(self.sprite_sheet, size, y1, size, size // 2,
                                                              angle=-90, colour=GREEN),
                                                    (x - self.x + size // 2, y - self.y)])
                        elif self.height == size:  # horizontal
                            if x == self.x:  # left
                                self.images.append(
                                    [get_image(self.sprite_sheet, 0, y1, size, size // 2, colour=GREEN),
                                     (x - self.x, y - self.y)])
                                self.images.append([get_image(self.sprite_sheet, 0, y1, size // 2, size,
                                                              angle=90, colour=GREEN),
                                                    (x - self.x, y - self.y + size // 2)])
                            elif x >= self.x + self.width - size:  # right
                                self.images.append([get_image(self.sprite_sheet, 0, y1, size // 2, size,
                                                              angle=-90, colour=GREEN),
                                                    (self.width - size, y - self.y)])
                                self.images.append([get_image(self.sprite_sheet, 0, y1, size, size // 2,
                                                              angle=180, colour=GREEN),
                                                    (self.width - size, y - self.y + size // 2)])
                            else:  # center
                                self.images.append(
                                    [get_image(self.sprite_sheet, size, y1, size, size // 2, colour=GREEN),
                                     (x - self.x, y - self.y)])
                                self.images.append([get_image(self.sprite_sheet, size, y1, size, size // 2,
                                                              angle=180, colour=GREEN),
                                                    (x - self.x, y - self.y + size // 2)])
                        elif x == self.x:  # left
                            if y == self.y:  # top corner
                                self.images.append(
                                    [get_image(self.sprite_sheet, 0, y1, size, size, colour=GREEN),
                                     (x - self.x, y - self.y)])
                            elif y >= self.y + self.height - size:  # bottom corner
                                self.images.append(
                                    [get_image(self.sprite_sheet, 0, y1, size, size, angle=90, colour=GREEN),
                                     (x - self.x, self.height - size)])
                            else:
                                self.images.append([
                                    get_image(self.sprite_sheet, size, y1, size, size, angle=90,
                                              colour=GREEN),
                                    (x - self.x, y - self.y)])
                        elif x >= self.x + self.width - size:  # right
                            if y == self.y:  # top corner
                                self.images.append(
                                    [get_image(self.sprite_sheet, 0, y1, size, size, angle=-90, colour=GREEN),
                                     (self.width - size, y - self.y)])
                            elif y >= self.y + self.height - size:  # bottom corner
                                self.images.append(
                                    [get_image(self.sprite_sheet, 0, y1, size, size, angle=180, colour=GREEN),
                                     (self.width - size, self.height - size)])
                            else:
                                self.images.append([
                                    get_image(self.sprite_sheet, size, y1, size, size, angle=-90,
                                              colour=GREEN),
                                    (self.width - size, y - self.y)])

                        elif y == self.y:  # top
                            self.images.append(
                                [get_image(self.sprite_sheet, size, y1, size, size, colour=GREEN),
                                 (x - self.x, y - self.y)])

                        elif y >= self.y + self.height - size:  # bottom
                            self.images.append([get_image(self.sprite_sheet, size, y1, size, size,
                                                          angle=180, colour=GREEN),
                                                (x - self.x, self.height - size)])

                        else:  # center
                            self.images.append(
                                [get_image(self.sprite_sheet, size * 2, y1, size, size, colour=GREEN),
                                 (x - self.x, y - self.y)])

    def movements(self, platforms_on_screen):
        on_screen = platforms_on_screen != []

        if self.around_spikes is None:
            self.around_spikes = [spike for spike in self.spikes if
                                  (collision([spike], [self.x - 1, self.y, 1, self.height]) and spike.width == PYXEL_SIZE // 2)
                                  or (collision([spike], [self.x + self.width, self.y, 1, self.height]) and spike.width == PYXEL_SIZE // 2)
                                  or (collision([spike], [self.x, self.y - 1, self.width, 1]) and spike.height == PYXEL_SIZE // 2)
                                  or (collision([spike], [self.x, self.y + self.height, self.width, 1]) and spike.height == PYXEL_SIZE // 2)]

        if self.direction == 1:  # right or left
            if self.speed > 0:  # right
                for k in range(self.speed):
                    if self.actual_move >= self.move:
                        self.actual_move = 0
                        self.speed *= -1
                        break
                    else:
                        self.x += 1
                        self.actual_move += 1
                        # move around spikes
                        for spike in self.around_spikes:
                            spike.x += 1

                        if on_screen:
                            # move player
                            if collision([self], [self.player.x, self.player.y + self.player.height, self.player.width, 1], True) and \
                                    not collision(platforms_on_screen, [self.player.x + self.player.width, self.player.y, 1,
                                                                   self.player.height]):
                                self.player.x += 1

                            # player at right side of the platform
                            elif collision([self], [self.player.x - 1, self.player.y, 1, self.player.width], True):
                                # player cant move
                                if collision(platforms_on_screen, [self.player.x + self.player.width, self.player.y, 1, self.player.height]):
                                    self.player.die = True
                                    break
                                else:
                                    self.player.x += 1
            else:  # left
                for k in range(-self.speed):
                    if self.actual_move >= self.move:
                        self.actual_move = 0
                        self.speed *= -1
                        break
                    else:
                        self.x -= 1
                        self.actual_move += 1
                        # move around spikes
                        for spike in self.around_spikes:
                            spike.x -= 1

                        if on_screen:
                            # move player
                            if collision([self], [self.player.x, self.player.y + self.player.height, self.player.width, 1], True) and \
                                    not collision(platforms_on_screen, [self.player.x - 1, self.player.y, 1,
                                                                   self.player.height]):
                                self.player.x -= 1

                            # player at left side of the platform
                            elif collision([self.player], [self.x - 1, self.y, 1, self.height], True):
                                # player cant move
                                if collision(platforms_on_screen,
                                             [self.player.x - 1, self.player.y, 1, self.player.height]):
                                    self.player.die = True
                                    break
                                else:
                                    self.player.x -= 1
        else:  # down or up
            if self.speed > 0:  # down
                for k in range(self.speed):
                    if self.actual_move >= self.move:
                        self.actual_move = 0
                        self.speed *= -1
                        break
                    else:
                        self.y += 1
                        self.actual_move += 1
                        # move around spikes
                        for spike in self.around_spikes:
                            spike.y += 1

                        if on_screen:
                            # move player
                            if collision([self], [self.player.x, self.player.y + self.player.height + 2, self.player.width, 1], True):
                                self.player.y += 1

                            # player under the platform
                            if collision([self], [self.player.x, self.player.y - 1, self.player.width, 1], True):
                                # player cant move
                                if collision(platforms_on_screen,
                                             [self.player.x, self.player.y + self.player.height, self.player.width, 1]):
                                    self.player.die = True
                                    break
                                else:
                                    self.player.y += 1
            else:  # up
                for k in range(-self.speed):
                    if self.actual_move >= self.move:
                        self.actual_move = 0
                        self.speed *= -1
                        break
                    else:
                        self.y -= 1
                        self.actual_move += 1
                        # move around spikes
                        for spike in self.around_spikes:
                            spike.y -= 1

                        if on_screen:
                            # player on top of the platform
                            if collision([self], [self.player.x, self.player.y + self.player.height, self.player.width, 1], True):
                                # player cant move
                                if collision(platforms_on_screen, [self.player.x, self.player.y - 1, self.player.width, 1]):
                                    self.player.die = True
                                    break
                                else:
                                    self.player.y -= 1

    def destroy(self):
        self.destruction = True
        if self.num_animation == 0:
            for spike in [spike for spike in self.spikes if collision([spike], [self.x - 1, self.y - 1, self.width + 2, self.height + 2], False)]:
                self.spikes.remove(spike)

        # animation
        if self.num_animation >= self.max_animation:
            # destroy

            self.platforms.remove(self)
        else:
            self.num_animation += 0.25
            self.init_images(int(self.num_animation) * PYXEL_SIZE)

    def red_blue(self):
        self.timer += 1

        if self.timer >= FPS * self.time_activate:
            if self.activate:
                self.activate = False
                self.init_images(PYXEL_SIZE)
            else:
                self.activate = True
                self.init_images()

            self.timer = 0

    def update(self):
        platforms_on_screen = items_in_screen(self.platforms)

        # moving platform
        if self.move != 0:
            self.movements(platforms_on_screen)

        # destructible
        if self.name == "destructible" and self.destruction:
            self.destroy()

        # red or blue
        if self.name in ["red", "blue"]:
            self.red_blue()

    def display(self, screen):
        if self.name in ["red", "blue"]:
            text = get_text(f"{self.time_activate - self.timer // FPS}", TEXT_SIZE, WHITE)
            text_rect = text.get_rect()
            screen.blit(text, (self.images[0][1][0] + self.x + self.width // 2 - text_rect.width // 2, self.images[0][1][1] + self.y + self.height // 2 - text_rect.height // 2))
        for img in self.images:
            screen.blit(img[0], (img[1][0] + self.x, img[1][1] + self.y))

        # display collision
        # pg.draw.rect(screen, RED, pg.Rect(self.x, self.y, self.width, self.height), 1)
