import pygame as pg

from parameters import *


class Door:
    def __init__(self, name,  x, y, player):
        self.name = name  # useless but have to (simplest)
        self.x, self.y, self.width, self.height = x, y, PYXEL_SIZE, PYXEL_SIZE
        self.player = player

        self.sprite_sheet = pg.image.load("../assets/Game_objects/door/door.png").convert_alpha()

        self.open = False

    def use_door(self, events) -> bool:
        for event in events:
            if is_key_pressed("interact", event):
                if collision([self.player], [self.x, self.y, self.width, self.height]):
                    # open door
                    if not self.open:
                        self.open = True
                        break

                    # enter in door
                    else:
                        return True
                return False
        return False

    def update(self): pass

    def display(self, screen):
        if self.open:
            screen.blit(get_image(self.sprite_sheet, PYXEL_SIZE, 0, self.width, self.height, colour=GREEN), (self.x, self.y))
        else:
            screen.blit(get_image(self.sprite_sheet, 0, 0, self.width, self.height, colour=GREEN), (self.x, self.y))
