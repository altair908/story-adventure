import pygame as pg

from parameters import *


class Decor:
    def __init__(self, name, x, y, width, height):
        self.name = name
        self.x, self.y, self.width, self.height = x, y, width, height

        self.sprite_sheet = pg.image.load(f"../assets/Game_objects/decor/{self.name}.png").convert_alpha()
        self.background = self.name[:2] == "bg"
        if self.background or self.name in []:
            self.sprite_size = 32
        else:
            self.sprite_size = 16

        # background or not
        if self.background:
            self.images = []
            self.init_images()

        # animations
        self.max_animation = self.sprite_sheet.get_size()[0] // self.width
        self.num_animation = 0

    def init_images(self):
        self.images = []
        if (self.width, self.height) == (PYXEL_SIZE, PYXEL_SIZE):
            # simple square
            angles = [0, -90, 90, -180]
            for k in range(4):
                self.images.append([get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE // 2, PYXEL_SIZE // 2,
                                              angle=angles[k], colour=GREEN),
                                    ((k % 2) * (PYXEL_SIZE // 2), (k // 2) * (PYXEL_SIZE // 2))])
        else:
            for x in range(self.x, self.x + self.width, PYXEL_SIZE):
                for y in range(self.y, self.y + self.height, PYXEL_SIZE):
                    if self.width == PYXEL_SIZE:  # vertical
                        if y == self.y:  # top
                            self.images.append(
                                [get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE // 2, PYXEL_SIZE, colour=GREEN),
                                 (x - self.x, y - self.y)])
                            self.images.append([get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE, PYXEL_SIZE // 2,
                                                          angle=-90, colour=GREEN),
                                                (x - self.x + PYXEL_SIZE // 2, y - self.y)])
                        elif y >= self.y + self.height - PYXEL_SIZE:  # bottom
                            self.images.append([get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE, PYXEL_SIZE // 2,
                                                          angle=90, colour=GREEN),
                                                (x - self.x, self.height - PYXEL_SIZE)])
                            self.images.append([get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE // 2, PYXEL_SIZE,
                                                          angle=180, colour=GREEN),
                                                (x - self.x + PYXEL_SIZE // 2, self.height - PYXEL_SIZE)])
                        else:  # center
                            self.images.append([get_image(self.sprite_sheet, PYXEL_SIZE, 0, PYXEL_SIZE, PYXEL_SIZE // 2,
                                                          angle=90, colour=GREEN), (x - self.x, y - self.y)])
                            self.images.append([get_image(self.sprite_sheet, PYXEL_SIZE, 0, PYXEL_SIZE, PYXEL_SIZE // 2,
                                                          angle=-90, colour=GREEN),
                                                (x - self.x + PYXEL_SIZE // 2, y - self.y)])
                    elif self.height == PYXEL_SIZE:  # horizontal
                        if x == self.x:  # left
                            self.images.append(
                                [get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE, PYXEL_SIZE // 2, colour=GREEN),
                                 (x - self.x, y - self.y)])
                            self.images.append([get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE // 2, PYXEL_SIZE,
                                                          angle=90, colour=GREEN),
                                                (x - self.x, y - self.y + PYXEL_SIZE // 2)])
                        elif x >= self.x + self.width - PYXEL_SIZE:  # right
                            self.images.append([get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE // 2, PYXEL_SIZE,
                                                          angle=-90, colour=GREEN),
                                                (self.width - PYXEL_SIZE, y - self.y)])
                            self.images.append([get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE, PYXEL_SIZE // 2,
                                                          angle=180, colour=GREEN),
                                                (self.width - PYXEL_SIZE, y - self.y + PYXEL_SIZE // 2)])
                        else:  # center
                            self.images.append(
                                [get_image(self.sprite_sheet, PYXEL_SIZE, 0, PYXEL_SIZE, PYXEL_SIZE // 2, colour=GREEN),
                                 (x - self.x, y - self.y)])
                            self.images.append([get_image(self.sprite_sheet, PYXEL_SIZE, 0, PYXEL_SIZE, PYXEL_SIZE // 2,
                                                          angle=180, colour=GREEN),
                                                (x - self.x, y - self.y + PYXEL_SIZE // 2)])
                    elif x == self.x:  # left
                        if y == self.y:  # top corner
                            self.images.append(
                                [get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE, PYXEL_SIZE, colour=GREEN),
                                 (x - self.x, y - self.y)])
                        elif y >= self.y + self.height - PYXEL_SIZE:  # bottom corner
                            self.images.append(
                                [get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE, PYXEL_SIZE, angle=90, colour=GREEN),
                                 (x - self.x, self.height - PYXEL_SIZE)])
                        else:
                            self.images.append([
                                get_image(self.sprite_sheet, PYXEL_SIZE, 0, PYXEL_SIZE, PYXEL_SIZE, angle=90,
                                          colour=GREEN),
                                (x - self.x, y - self.y)])
                    elif x >= self.x + self.width - PYXEL_SIZE:  # right
                        if y == self.y:  # top corner
                            self.images.append(
                                [get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE, PYXEL_SIZE, angle=-90, colour=GREEN),
                                 (self.width - PYXEL_SIZE, y - self.y)])
                        elif y >= self.y + self.height - PYXEL_SIZE:  # bottom corner
                            self.images.append(
                                [get_image(self.sprite_sheet, 0, 0, PYXEL_SIZE, PYXEL_SIZE, angle=180, colour=GREEN),
                                 (self.width - PYXEL_SIZE, self.height - PYXEL_SIZE)])
                        else:
                            self.images.append([
                                get_image(self.sprite_sheet, PYXEL_SIZE, 0, PYXEL_SIZE, PYXEL_SIZE, angle=-90,
                                          colour=GREEN),
                                (self.width - PYXEL_SIZE, y - self.y)])

                    elif y == self.y:  # top
                        self.images.append(
                            [get_image(self.sprite_sheet, PYXEL_SIZE, 0, PYXEL_SIZE, PYXEL_SIZE, colour=GREEN),
                             (x - self.x, y - self.y)])

                    elif y >= self.y + self.height - PYXEL_SIZE:  # bottom
                        self.images.append([get_image(self.sprite_sheet, PYXEL_SIZE, 0, PYXEL_SIZE, PYXEL_SIZE,
                                                      angle=180, colour=GREEN),
                                            (x - self.x, self.height - PYXEL_SIZE)])

                    else:  # center
                        self.images.append(
                            [get_image(self.sprite_sheet, PYXEL_SIZE * 2, 0, PYXEL_SIZE, PYXEL_SIZE, colour=GREEN),
                             (x - self.x, y - self.y)])

    def update(self):
        if not self.background and 0 <= self.x <= SCREEN_SIZE[0] and 0 <= self.y <= SCREEN_SIZE[1]:  # on screen
            # animations
            self.num_animation += 0.1
            if self.num_animation >= self.max_animation:
                self.num_animation = 0

    def display(self, screen):
        if self.background:
            for img in self.images:
                screen.blit(img[0], (img[1][0] + self.x, img[1][1] + self.y))
        else:
            for x in range(self.x, self.x + self.width - 1, self.sprite_size):
                for y in range(self.y, self.y + self.height - 1, self.sprite_size):
                    screen.blit(get_image(self.sprite_sheet, int(self.num_animation) * self.sprite_size, 0, self.sprite_size, self.sprite_size, colour=GREEN), (x, y))
