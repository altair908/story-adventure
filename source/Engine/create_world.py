import pygame as pg
import os

from parameters import *
from Game_objects.camera import Camera
from Game_objects.player import Player
from Game_objects.platform import Platform
from Game_objects.spike import Spike
from Game_objects.door import Door
from Game_objects.decor import Decor
from Game_objects.sign import Sign


def add_level(num: int, world: list[list], spawn_point: list) -> None:
    with open(f"../assets/Levels/{num}.txt", "w+") as file:
        file.write(f"{spawn_point[0]};{spawn_point[1]};\n")
        for i in range(len(world)):
            for item in world[i]:
                if i == 1:  # platforms
                    file.write(f"{type(item).__name__};{item.name};{item.x};{item.y};{item.width};{item.height};{item.move*item.direction};\n")
                elif i == 4:  # signs
                    file.write(f"{type(item).__name__};{item.name};{item.x};{item.y};{item.width};{item.height};{item.base_text};\n")
                else:
                    file.write(f"{type(item).__name__};{item.name};{item.x};{item.y};{item.width};{item.height};\n")


class MovingPlatform:
    def __init__(self, x, y, width, height):
        self.x, self.y, self.width, self.height = x, y, width, height

    def display(self, screen):
        screen.blit(pg.image.load("assets/arrow.png"), (self.x, self.y))


class SpawnPoint:
    def __init__(self, x, y):
        self.x, self.y, self.width, self.height = x, y, PYXEL_SIZE, PYXEL_SIZE

    def display(self, screen):
        screen.blit(get_image(pg.image.load("assets/spawn_point.png").convert_alpha(), 0, 0, self.width, self.height, colour=GREEN), (self.x, self.y))


class Game:
    def __init__(self):
        # pygame initialisation
        pg.init()
        pg.display.set_caption("Platformer")
        pg.display.set_icon(pg.image.load("../assets/Icon/icon.png"))
        self.added_width = PYXEL_SIZE * 5
        self.screen = pg.display.set_mode((SCREEN_SIZE[0] + self.added_width, SCREEN_SIZE[1]), -214748364)

        self.level = 16

        # fps
        self.clock = pg.time.Clock()

        # Global
        self.spawn_point = SpawnPoint(PYXEL_SIZE, PYXEL_SIZE)
        self.decors = []
        self.platforms = []
        self.spikes = []
        self.doors = []
        self.signs = []
        self.world = [self.decors, self.platforms, self.spikes, self.doors, self.signs]

        # simplest
        self.player = Player(0, 0, self.world, self.level)

        self.init_world()

        # select pieces
        self.selection = [SpawnPoint(0, 0),
                          MovingPlatform(0, 0, PYXEL_SIZE, PYXEL_SIZE)]
        for obj in ["platform", "spike", "door", "decor", "sign"]:
            for name in self.get_name(obj):
                match obj:
                    case "platform":
                        if name == "bridge":
                            item = Platform(name, 0, 4, PYXEL_SIZE, PYXEL_SIZE // 2, self.world, self.player)
                        else:
                            item = Platform(name, 0, 0, PYXEL_SIZE, PYXEL_SIZE, self.world, self.player)
                    case "spike":
                        item = Spike(name, 8, 8, PYXEL_SIZE // 2, PYXEL_SIZE // 2, self.world)
                    case "door":
                        item = Door(name, 0, 0, self.player)
                    case "decor":
                        if name[:2] == "bg":  # background
                            item = Decor(name, 0, 0, PYXEL_SIZE, PYXEL_SIZE)
                        else:
                            item = Decor(name, 8, 8, PYXEL_SIZE // 2, PYXEL_SIZE // 2)
                    case "sign":
                        if name == "help":
                            item = Sign("help", 8, 8, PYXEL_SIZE // 2, PYXEL_SIZE // 2, self.player, "")
                        else:
                            item = Sign(name, 0, 0, PYXEL_SIZE, PYXEL_SIZE, self.player, "")
                            if name == "ancient":
                                item.animation = 1
                    case _:
                        item = None
                self.selection.append(item)

        # put at the right place
        for i in range(len(self.selection)):
            self.selection[i].x += SCREEN_SIZE[0] + PYXEL_SIZE * (i % (self.added_width // PYXEL_SIZE))
            self.selection[i].y += PYXEL_SIZE * (i // (self.added_width // PYXEL_SIZE))

        self.coord_in_selection = (SCREEN_SIZE[0], 0)
        self.select = self.selection[0]
        self.start, self.end = (), ()  # start and end of our selection

        # camera
        self.reset_time_camera = FPS // 6
        self.time_camera = 0
        self.camera_speed = PYXEL_SIZE
        self.camera = Camera(self.player, self.world, self.spawn_point)

        self.main()

    @staticmethod
    def get_name(objet: str) -> list:
        return [f[:-4] for f in os.listdir(f'../assets/Game_objects/{objet}') if
                os.path.isfile(os.path.join(f'../assets/Game_objects/{objet}', f))]

    def init_world(self) -> None:
        self.spawn_point = SpawnPoint(PYXEL_SIZE, PYXEL_SIZE)
        self.decors = []
        self.platforms = []
        self.spikes = []
        self.doors = []
        self.signs = []
        self.world = [self.decors, self.platforms, self.spikes, self.doors, self.signs]

        world, spawn_point = get_level(self.level)
        self.spawn_point = SpawnPoint(int(spawn_point[0]), int(spawn_point[1]))
        for item in world:
            if item[0] == "Decor":
                self.decors.append(Decor(item[1], int(item[2]), int(item[3]), int(item[4]), int(item[5])))
            elif item[0] == "Platform":
                obj = Platform(item[1], int(item[2]), int(item[3]), int(item[4]), int(item[5]), self.world, self.player, int(item[6]))
                if obj.name in ["red", "blue"]:
                    obj.activate = True
                self.platforms.append(obj)
            elif item[0] == "Spike":
                self.spikes.append(Spike(item[1], int(item[2]), int(item[3]), int(item[4]), int(item[5]), self.world))
            elif item[0] == "Door":
                self.doors.append(Door(item[1], int(item[2]), int(item[3]), self.player))
            elif item[0] == "Sign":
                s = Sign(item[1], int(item[2]), int(item[3]), int(item[4]), int(item[5]), self.player, item[6])
                if s.name == "ancient":
                    s.animation = 1
                self.signs.append(s)

    def place(self):
        w, h = self.end[0] + self.select.width - self.start[0], self.end[1] + self.select.height - self.start[1]
        abs_w, abs_h = abs(w), abs(h)
        if w < 0:
            tmp = self.start[0]
            self.start = (self.end[0], self.start[1])
            self.end = (tmp, self.end[1])
            abs_w += PYXEL_SIZE * 2
        if h < 0:
            tmp = self.start[1]
            self.start = (self.start[0], self.end[1])
            self.end = (self.end[0], tmp)
            abs_h += PYXEL_SIZE * 2

        if type(self.select) is SpawnPoint:
            self.spawn_point = SpawnPoint(self.start[0], self.start[1])
            self.camera.spawn_point = self.spawn_point
        elif type(self.select) is MovingPlatform:
            for platform in self.platforms:
                if collision([platform], [self.start[0], self.start[1], abs_w, abs_h]):
                    if abs_w > abs_h:  # right or left
                        platform.move = self.end[0] - platform.x - platform.width + self.select.width
                        platform.direction = 1
                    else:  # up or down
                        platform.move = self.end[1] - platform.y - platform.height + self.select.height
                        platform.direction = -1
                    break
        elif type(self.select) is Platform:
            if not collision(self.platforms + self.spikes + self.doors, [self.start[0], self.start[1], abs_w, abs_h]):
                obj = Platform(self.select.name, self.start[0], self.start[1], abs_w, abs_h, self.world, self.player, self.select.move)
                if obj.name in ["red", "blue"]:
                    obj.activate = True
                self.platforms.append(obj)
        elif type(self.select) is Spike:
            if not collision(self.platforms + self.spikes + self.doors, [self.start[0], self.start[1], abs_w, abs_h]):
                self.spikes.append(Spike(self.select.name, self.start[0], self.start[1], abs_w, abs_h, self.world))
        elif type(self.select) is Door:
            if not collision(self.platforms + self.spikes + self.doors, [self.start[0], self.start[1], abs_w, abs_h]):
                self.doors.append(Door(self.select.name, self.start[0], self.start[1], self.player))
        elif type(self.select) is Decor:
            self.decors.append(Decor(self.select.name, self.start[0], self.start[1], abs_w, abs_h))
        elif type(self.select) is Sign:
            s = Sign(self.select.name, self.start[0], self.start[1], PYXEL_SIZE, PYXEL_SIZE, self.player, self.select.base_text)
            if s.name == "ancient":
                s.animation = 1
            self.signs.append(s)

    def update(self, events, x, y):
        # next/precedent level
        for event in events:
            if event.type == pg.KEYDOWN and (event.key == pg.K_RIGHT or event.key == pg.K_LEFT):
                # next
                if event.key == pg.K_RIGHT:
                    self.level += 1
                    self.init_world()

                    self.reset_time_camera = FPS // 6
                    self.time_camera = 0
                    self.camera_speed = PYXEL_SIZE
                    self.camera = Camera(self.player, self.world, self.spawn_point)
                # precedent
                elif event.key == pg.K_LEFT and self.level > 0:
                    self.level -= 1
                    self.init_world()

                    self.reset_time_camera = FPS // 6
                    self.time_camera = 0
                    self.camera_speed = PYXEL_SIZE
                    self.camera = Camera(self.player, self.world, self.spawn_point)
                break

        # screenshot
        keys = pg.key.get_pressed()
        if keys[pg.K_p]:
            screenshot = pg.Surface(self.screen.get_size())
            screenshot.blit(self.screen, (0, 0))
            pg.image.save(screenshot, "assets/Menu/Background/bg.png")

        # camera
        if self.time_camera <= 0:
            if keys[pg.K_q]:
                self.camera.move_left(self.camera_speed)
                if keys[pg.K_LSHIFT]:
                    self.time_camera = self.reset_time_camera // 2
                else:
                    self.time_camera = self.reset_time_camera
            if keys[pg.K_d]:
                self.camera.move_right(self.camera_speed)
                if keys[pg.K_LSHIFT]:
                    self.time_camera = self.reset_time_camera // 2
                else:
                    self.time_camera = self.reset_time_camera
            if keys[pg.K_z]:
                self.camera.move_up(self.camera_speed)
                if keys[pg.K_LSHIFT]:
                    self.time_camera = self.reset_time_camera // 2
                else:
                    self.time_camera = self.reset_time_camera
            if keys[pg.K_s]:
                self.camera.move_down(self.camera_speed)
                if keys[pg.K_LSHIFT]:
                    self.time_camera = self.reset_time_camera // 2
                else:
                    self.time_camera = self.reset_time_camera
        else:
            self.time_camera -= 1

        # left clic
        if pg.mouse.get_pressed()[0]:
            x_menu, y_menu = pg.mouse.get_pos()
            if x >= SCREEN_SIZE[0]:  # selection in menu
                for item in self.selection:
                    if item.x <= x_menu <= item.x + PYXEL_SIZE and item.y <= y_menu <= item.y + PYXEL_SIZE:
                        self.select = item
                        self.coord_in_selection = (item.x // PYXEL_SIZE * PYXEL_SIZE, item.y // PYXEL_SIZE * PYXEL_SIZE)
                        break
            else:  # start selection
                for event in events:
                    if event.type == pg.MOUSEBUTTONDOWN and event.button == pg.BUTTON_LEFT:
                        self.start = (x, y)
                        break

        elif self.end == ():  # end selection
            if x < SCREEN_SIZE[0]:
                for event in events:
                    if event.type == pg.MOUSEBUTTONUP and event.button == pg.BUTTON_LEFT:
                        self.end = (x, y)
                        self.place()
                        self.start, self.end = (), ()
                        break

        # right clic
        if pg.mouse.get_pressed()[2]:
            if type(self.select) is Decor:
                for item in self.decors:
                    if collision([item], [x, y, self.select.width, self.select.height]):
                        self.decors.remove(item)
            else:
                for items in self.world:
                    for item in items:
                        if collision([item], [x, y, self.select.width, self.select.height]):
                            items.remove(item)

        if pg.key.get_pressed()[pg.K_SPACE]:
            self.camera.reset()
            add_level(self.level, self.world, [self.spawn_point.x, self.spawn_point.y])

    def display(self, x, y):
        pg.draw.rect(self.screen, BLACK, pg.Rect(0, 0, SCREEN_SIZE[0], SCREEN_SIZE[1]))
        # self.screen.blit(pg.transform.scale(pg.image.load("assets/Backgrounds/2.jpg"), (SCREEN_SIZE[0], SCREEN_SIZE[1])), (self.camera.x, self.camera.y))

        # draw world
        for tab in self.world:
            for item in tab:
                item.display(self.screen)

        self.spawn_point.display(self.screen)

        # draw where you are drawing
        if x < SCREEN_SIZE[0]:
            if self.start != ():
                tmp_start = self.start
                tmp_end = (x, y)
                w, h = tmp_end[0] + self.select.width - tmp_start[0], tmp_end[1] + self.select.height - tmp_start[1]
                abs_w, abs_h = abs(w), abs(h)
                if w < 0:
                    tmp = tmp_start[0]
                    tmp_start = (tmp_end[0], tmp_start[1])
                    tmp_end = (tmp, tmp_end[1])
                    if abs_w >= self.select.width:
                        abs_w += self.select.width * 2
                if h < 0:
                    tmp = tmp_start[1]
                    tmp_start = (tmp_start[0], tmp_end[1])
                    tmp_end = (tmp_end[0], tmp)
                    if abs_h >= self.select.height:
                        abs_h += self.select.height * 2
                pg.draw.rect(self.screen, RED, pg.Rect(tmp_start[0], tmp_start[1], abs_w, abs_h), 1)
            else:
                pg.draw.rect(self.screen, RED, pg.Rect(x, y, self.select.width, self.select.height), 1)

        pg.draw.rect(self.screen, WHITE, pg.Rect(SCREEN_SIZE[0], 0, self.added_width, SCREEN_SIZE[1]))

        # draw selection pieces
        for item in self.selection:
            item.display(self.screen)

        # draw rect around the selected piece in the selection
        pg.draw.rect(self.screen, RED, pg.Rect(self.coord_in_selection[0], self.coord_in_selection[1], PYXEL_SIZE, PYXEL_SIZE), 1)

        txt = get_text(str(self.level), TEXT_SIZE, BLACK)
        self.screen.blit(txt, (SCREEN_SIZE[0] + self.added_width // 2 - txt.get_width() // 2, SCREEN_SIZE[1] - PYXEL_SIZE))

    def main(self):
        finish = False
        while not finish:
            events = pg.event.get()
            for event in events:
                if event.type == pg.QUIT or event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
                    finish = True

            x, y = pg.mouse.get_pos()
            if not pg.key.get_pressed()[pg.K_LSHIFT]:
                x, y = x // self.select.width * self.select.width, y // self.select.height * self.select.height
            else:
                w, h = self.select.width // 2, self.select.height // 2
                x, y = x // w * w, y // h * h

            self.update(events, x, y)
            self.display(x, y)

            pg.display.flip()
            self.clock.tick(FPS)

        pg.quit()


Game()
