import pygame as pg


# WINDOW
PYXEL_SIZE = 32
SCREEN_SIZE = (PYXEL_SIZE * 17, PYXEL_SIZE * 11)  # 17 / 11  => 70 / 40
FPS = 60


# PLAYER ANIMATIONS
ANIMATION_TIME = {"idle": 0.1, "run": 0.2, "jump": 0.3, "fall": 0.1, "dash": 0.3, "crouch": 0.1, "slide": 0.1, "after_slide": 0.1}
PLAYER_ANIMATIONS = {"idle": [(8, 4), (58, 4), (108, 4), (158, 4)],
                     "run": [(58, 40), (108, 40), (158, 40), (208, 40), (258, 40), (308, 40)],
                     "jump": [(108, 76), (158, 76), (208, 76), (258, 76), (308, 76), (8, 112), (58, 112)],
                     "fall": [(58, 112), (108, 112)],
                     "dash": [(308, 338), (8, 374), (58, 374)],
                     "crouch": [(208, 12), (258, 12), (308, 12), (8, 49)],
                     "slide": [(158, 131), (208, 131)],
                     "after_slide": [(308, 112, (8, 131))]}


# TEXT
TEXT_SIZE = 16


# COLORS
WHITE = [255, 255, 255]
BLACK = [0, 0, 0]
RED = [255, 0, 0]
GREEN = [0, 255, 0]
BLUE = [0, 0, 255]


def write_save(d: dict) -> None:
    with open("../assets/Save/game.txt", "w+") as file:
        for key, val in d.items():
            file.write(f"{key};{str(val)};\n")


def get_save() -> dict:
    with open("../assets/Save/game.txt", "r+") as file:
        dico = {}
        for line in file:
            word, key = "", ""
            for char in line:
                if char == ";":
                    if key == "":
                        key = word
                        word = ""
                    else:
                        dico[key] = int(word)
                else:
                    word += char
    return dico


def write_keys(keys: dict) -> None:
    with open("../assets/Save/keys.txt", "w+") as file:
        for key, val in keys.items():
            file.write(f"{key};{val};\n")


def get_keys() -> dict:
    with open("../assets/Save/keys.txt", "r+") as file:
        dico = {}
        for line in file:
            word, key = "", ""
            for char in line:
                if char == ";":
                    if key == "":
                        key = word
                        word = ""
                    else:
                        dico[key] = word
                else:
                    word += char
    return dico


# PLAYER KEYS
KEYS = get_keys()


def is_key_pressed(action_name: str, event=None) -> bool:
    key_name = KEYS[action_name]
    if key_name[:5] == "mouse":
        if event is not None:  # with events
            if event.type == pg.MOUSEBUTTONDOWN:
                match key_name[6:]:
                    case "left":
                        return event.button == 1
                    case "wheel":
                        return event.button == 2
                    case "right":
                        return event.button == 3
        else:
            match key_name[6:]:
                case "left":
                    return pg.mouse.get_pressed()[0]
                case "wheel":
                    return pg.mouse.get_pressed()[1]
                case "right":
                    return pg.mouse.get_pressed()[2]
    else:
        if event is not None:  # with events
            return event.type == pg.KEYDOWN and pg.key.name(event.key) == key_name
        key_code = pg.key.key_code(key_name)
        pressed_keys = pg.key.get_pressed()
        return pressed_keys[key_code]

    return False


def not_names(items: list, names: list) -> list:
    return [item for item in items if item.name not in names]


def is_names(items: list, names: list) -> list:
    return [item for item in items if item.name in names]


def items_in_screen(items: list) -> list:
    tab = []
    for item in items:
        if 0 <= item.x <= SCREEN_SIZE[0] or -1 <= item.x + item.width <= SCREEN_SIZE[0]:
            if 0 <= item.y <= SCREEN_SIZE[1] or -1 <= item.y + item.height <= SCREEN_SIZE[1]:
                tab.append(item)
    return tab


def collision(items: list, coord: list[int] | tuple, only_screen=False) -> bool:
    if only_screen:
        items = items_in_screen(items)
    for item in items:
        running = True
        if hasattr(item, "name") and item.name in ["red", "blue"]:
            if not item.activate:
                running = False

        for x in range(coord[0], coord[0] + coord[2]):
            if not running:
                break
            for x_i in range(item.x, item.x + item.width):
                if x == x_i:
                    for y in range(coord[1], coord[1] + coord[3]):
                        for y_i in range(item.y, item.y + item.height):
                            if y == y_i:
                                return True
                    running = False
                    break
    return False


def get_level(num: int) -> tuple:
    try:
        with open(f"../assets/Levels/{num}.txt", "r+") as file:
            world, spawn_point = [], []
            tab_temp = []
            first = True
            for line in file:
                temp = ""
                for e in line:
                    if e == ";":
                        tab_temp.append(temp)
                        temp = ""
                    else:
                        temp += e

                if not first:
                    world.append(tab_temp)
                else:
                    first = False
                    spawn_point = tab_temp
                tab_temp = []
            if spawn_point == []:
                spawn_point = [PYXEL_SIZE, PYXEL_SIZE]
            return world, spawn_point
    except FileNotFoundError:
        with open(f"../assets/Levels/{num}.txt", "w+") as file:
            pass
        return [], [PYXEL_SIZE, PYXEL_SIZE]


def get_text(text: str, size, color):
    font = pg.font.Font(None, size)
    return font.render(text, True, color)


def get_image(sheet, x, y, width, height, angle=0, colour=None, scale=1, reverse=False):
    width, height = width/scale, height/scale
    img = pg.Surface((width, height)).convert_alpha()
    img.blit(sheet, (0, 0), (x, y, width, height))
    img = pg.transform.scale(img, (width * scale, height * scale))
    img = pg.transform.rotate(img, angle)
    if reverse:
        img = pg.transform.flip(img, 1, 0)
    if colour is not None:
        img.set_colorkey(colour)
    return img
