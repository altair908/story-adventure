import pygame as pg

from parameters import *


class Spike:
    def __init__(self, name, x, y, width, height, world):
        self.name = name
        self.pyxel_size = PYXEL_SIZE // 2
        self.x, self.y, self.width, self.height = x, y, width // self.pyxel_size * self.pyxel_size, height // self.pyxel_size * self.pyxel_size
        self.platforms = world[1]

        # for invisible and blue/red platforms
        self.displaying = True

        # load the image
        self.img = pg.image.load(f"assets/Game_objects/spike/{self.name}.png")
        self.init_images()

    def init_images(self):
        # load the image
        if (self.width, self.height) == (self.pyxel_size, self.pyxel_size):  # just 1 spike
            if collision(self.platforms, [self.x, self.y - 1, self.width, 1]):  # pointing down
                self.img = pg.transform.rotate(self.img, 180)
            elif collision(self.platforms, [self.x - 1, self.y, 1, self.height]):  # pointing right
                self.img = pg.transform.rotate(self.img, -90)
            elif collision(self.platforms, [self.x + self.width, self.y, 1, self.height]):  # pointing left
                self.img = pg.transform.rotate(self.img, 90)
        # platform on top / left / right / bottom
        elif self.height == self.pyxel_size:  # horizontal
            if collision(self.platforms, [self.x, self.y - 1, self.width, 1]):  # pointing down
                self.img = pg.transform.rotate(self.img, 180)
        elif self.width == self.pyxel_size:  # vertical
            if collision(self.platforms, [self.x - 1, self.y, 1, self.height]):  # pointing right
                self.img = pg.transform.rotate(self.img, -90)
            elif collision(self.platforms, [self.x + self.width, self.y, 1, self.height]):  # pointing left
                self.img = pg.transform.rotate(self.img, 90)

    def update(self):
        pass

    def display(self, screen):
        if self.displaying:
            for x in range(self.x, self.x + self.width, self.pyxel_size):
                for y in range(self.y, self.y + self.height, self.pyxel_size):
                    screen.blit(pg.transform.scale(self.img, (self.pyxel_size, self.pyxel_size)), (x, y))
