import pygame as pg

from parameters import *


class Sign:
    def __init__(self, name, x, y, width, height, player, text):
        self.name = name
        self.x, self.y, self.width, self.height = x, y, width, height
        self.player = player

        self.len_line = 16
        self.base_text = text
        self.text = []
        self.create_text()

        self.render = False

        # ancient
        if self.name == "ancient":
            self.direction = 1
            self.animation = -1  # 0 / 1 / 2

        # vision
        if self.name == "vision":
            self.activate = False
            self.animation = 0  # 0 -> close / 1 -> closing / 2 -> open / 3 -> opening

        self.sprite_sheet = pg.image.load(f"assets/Game_objects/sign/{self.name}.png").convert_alpha()
        if self.name in ["help"]:
            self.sprite_size = 16
        else:
            self.sprite_size = 32

        # animations
        self.max_animation = self.sprite_sheet.get_size()[0] // self.width
        self.num_animation = 0

    def create_text(self) -> None:
        self.text = []
        temp_text, temp_len, key = "", 0, ""
        for i in range(len(self.base_text)):
            if temp_len >= self.len_line and self.base_text[i] == " ":
                self.text.append(temp_text)
                temp_text = ""
                temp_len = 0
            else:
                # add a key
                if key != "" or self.base_text[i] == "$":
                    if self.base_text[i] == "$" and key != "":  # end selection
                        temp_text += KEYS[key[1:]]
                        key = ""
                    else:
                        key += self.base_text[i]
                else:
                    temp_text += self.base_text[i]
                temp_len += 1
        self.text.append(temp_text)

    def ancient(self):
        # look left or right
        x_dist, y_dist = PYXEL_SIZE * 4, PYXEL_SIZE * 3  # dist on both sides / dist on top of the ancient
        if (self.y - y_dist <= self.player.y + self.player.height // 2 <= self.y + self.height and
                self.x - x_dist <= self.player.x + self.player.width // 2 <= self.x + self.width + x_dist):
            # passing to next animation
            if self.animation < 0:
                self.animation = 0

            # direction
            ans = self.player.x - self.x
            if ans != 0:
                self.direction = ans // abs(ans)

        elif self.animation == 1:
            self.animation = 2
            self.num_animation = 0

    def update(self):
        if self.name == "ancient":
            self.ancient()

        if items_in_screen([self]) != []:  # on screen
            # collision with player (display the text)
            if (((self.name == "ancient" and self.animation == 1) or self.name != "ancient") and
                    collision([self.player], (self.x + 2, self.y + self.height // 2, self.width - 6, 1))):
                self.render = True

            elif self.render:
                self.render = False
                # self.num_animation = 0

            # animations
            if self.name == "ancient":
                if self.animation >= 0:
                    self.num_animation += 0.18
                    if self.animation == 1:
                        self.num_animation -= 0.08
                    elif self.num_animation >= 13:
                        if self.animation == 0:
                            self.animation = 1
                        else:
                            self.animation = -1

                        self.num_animation = 0
            elif self.name == "vision":
                if self.animation in [1, 3]:  # opening or closing
                    self.num_animation += 0.17

                    if self.num_animation >= self.max_animation:
                        self.animation -= 1

                # player use the vision
                elif self.activate:
                    if self.animation != 2:  # != open
                        self.animation = 3  # opening

                # player stop vision
                elif self.animation != 0:  # != close
                    self.animation = 1  # closing

            else:
                self.num_animation += 0.08

            # reset animation
            if self.num_animation >= self.max_animation:
                self.num_animation = 0

    def display(self, screen):
        if self.name == "ancient":
            if self.animation >= 0:
                screen.blit(get_image(self.sprite_sheet, int(self.num_animation) * self.sprite_size, PYXEL_SIZE * self.animation, self.sprite_size,
                                      self.sprite_size, colour=GREEN, reverse=(self.direction == -1)), (self.x, self.y))
        elif self.name == "vision":
            match self.animation:
                case 0:  # close
                    x, y = 0, 0
                case 1:  # closing
                    x, y = int(self.num_animation) * self.sprite_size, PYXEL_SIZE
                case 2:  # open
                    x, y = 0, PYXEL_SIZE
                case _:  # opening
                    x, y = int(self.num_animation) * self.sprite_size, 0
            screen.blit(get_image(self.sprite_sheet, x, y, self.sprite_size, self.sprite_size, colour=GREEN), (self.x, self.y))
        else:
            screen.blit(get_image(self.sprite_sheet, int(self.num_animation) * self.sprite_size, 0, self.sprite_size,
                                  self.sprite_size, colour=GREEN), (self.x, self.y))

        if self.render:
            for i in range(len(self.text)):
                text = get_text(self.text[i], TEXT_SIZE, WHITE)
                text_rect = text.get_rect()
                if self.sprite_size == 32:
                    if self.name in ["ancient", "vision"]:
                        screen.blit(text, (self.x + self.width // 2 - text_rect.width // 2,
                                           self.y + 10 - (len(self.text) - i) * 10 - text_rect.height))
                    else:
                        screen.blit(text, (self.x + self.width // 2 - text_rect.width // 2,
                                           self.y + 10 - (len(self.text) - i) * 10 - text_rect.height // 2))
                else:
                    screen.blit(text, (self.x + self.width // 2 - text_rect.width // 2, self.y - (len(self.text) - i) * 10 - text_rect.height // 2))
