import pygame as pg

from parameters import *


class Player:
    def __init__(self, x, y, world, level):
        self.x, self.y, self.width, self.height = x + 4, y, PYXEL_SIZE - 8, PYXEL_SIZE

        # self.world = world
        self.doors = world[0]
        self.platforms = world[1]
        self.spikes = world[2]
        self.signs = world[4]

        self.sprite_sheet = pg.image.load("assets/Game_objects/player/player.png").convert_alpha()

        # death when crush by a moving platform
        self.die = False

        # animations
        self.animation = "idle"
        self.num_animation = 0
        self.direction = 1  # -1: left / 1: right

        # STATS
        # movements
        self.reset_gravity = 0
        self.gravity = self.reset_gravity
        self.speed = 3
        self.double_jump_number = 1
        self.dash_number = 1
        if 0 < level <= 3:
            self.double_jump_number = 0
            self.dash_number = 0
        elif level == 4:
            self.dash_number = 0

        # dash
        self.dash_distance = 120
        self.dash_speed = 13
        self.reset_time_dash = 10
        self.actual_dash_number = 0
        self.actual_dash = 0
        self.dashing = False
        self.time_dash = self.reset_time_dash

        # jump
        self.jump_height = 80
        self.jump_speed = 5
        self.actual_jump = 0
        self.jumping, self.possibility_double_jump = False, [False, 0]  # can double jump/times that you jump in midair
        self.can_wall_jump = [False, False]  # left / right
        self.reset_project_speed = 4  # wall jump and destructible
        self.project_speed = 0

        # crouch and slide
        self.crouching = False
        self.min_movement_time = FPS // 2  # 0.5 sec
        self.movement_time = 0  # going to min_running_time
        self.sliding = False
        self.reset_slide_speed = 5
        self.slide_speed = self.reset_slide_speed

        # ice
        self.ice_speed = self.speed
        self.ice_move = 0  # between -speed and speed

        # destructible
        self.destruction = False

        # invisible
        self.vision = False
        self.activate_eye = None

        self.platforms_on_screen = []

    def get_vision(self, events) -> None:
        for event in events:
            if is_key_pressed("interact", event):
                for sign in items_in_screen(is_names(self.signs, ["vision"])):
                    if collision([sign], [self.x, self.y, self.width, self.height]):
                        if self.vision:
                            self.vision = False
                            sign.activate = False
                        else:
                            self.vision = True
                            sign.activate = True
                        self.activate_eye = sign
                        return

    def change_animation(self, name: str) -> None:
        if self.animation != name and self.animation not in ["crouch", "slide"]:
            self.num_animation = 0
            self.animation = name

    def dying(self) -> bool:
        return (self.die or
                collision(self.spikes, [self.x + 8, self.y + 2, self.width - 16, self.height - 4]) or
                collision(is_names(self.platforms_on_screen, ["red", "blue"]),
                          [self.x, self.y, self.width, self.height]))

    def left_and_right(self):
        if not is_key_pressed("left") and not is_key_pressed("right"):
            self.movement_time = 0
        else:
            platform_not_bridge = not_names(self.platforms_on_screen, ["bridge"])

            if is_key_pressed("left"):  # left
                if self.movement_time < self.min_movement_time and collision(platform_not_bridge,
                                                                             [self.x, self.y + self.height, self.width,
                                                                              1]):
                    # slide
                    if self.animation == "run" and self.direction == -1:
                        self.movement_time += 1
                    else:
                        self.movement_time = 0

                self.direction = -1
                if self.crouching:
                    speed = 1
                else:
                    speed = self.speed

                if not collision(platform_not_bridge, [self.x - speed, self.y, speed, self.height]):
                    self.x -= speed
                    if collision(is_names(platform_not_bridge, ["ice"]),
                                 [self.x, self.y + self.height, self.width, 1]):
                        self.ice_move = -self.ice_speed
                else:
                    ice = False
                    for k in range(speed - 1):
                        if not collision(platform_not_bridge, [self.x - 1, self.y, 1, self.height]):
                            self.x -= 1
                            if not ice and collision(is_names(platform_not_bridge, ["ice"]),
                                                     [self.x, self.y + self.height, self.width, 1]):
                                self.ice_move = -self.ice_speed
                                ice = True
                        else:
                            break
            if is_key_pressed("right"):  # right
                if self.movement_time < self.min_movement_time and collision(platform_not_bridge,
                                                                             [self.x, self.y + self.height, self.width,
                                                                              1]):
                    # slide
                    if self.animation == "run" and self.direction == 1:
                        self.movement_time += 1
                    else:
                        self.movement_time = 0

                self.direction = 1
                if self.crouching:
                    speed = 1
                else:
                    speed = self.speed

                if not collision(platform_not_bridge, [self.x + self.width, self.y, speed, self.height]):
                    self.x += speed
                    if collision(is_names(platform_not_bridge, ["ice"]),
                                 [self.x, self.y + self.height, self.width, 1]):
                        self.ice_move = self.ice_speed
                else:
                    ice = False
                    for k in range(speed - 1):
                        if not collision(platform_not_bridge, [self.x + self.width, self.y, 1, self.height]):
                            self.x += 1
                            if not ice and collision(is_names(platform_not_bridge, ["ice"]),
                                                     [self.x, self.y + self.height, self.width, 1]):  # ice
                                self.ice_move = self.ice_speed
                                ice = True
                        else:
                            break

    def jump(self):
        if self.project_speed != 0:  # wall jump
            prj_speed = abs(int(self.project_speed))
            if self.project_speed < 0:  # right
                if not collision(self.platforms_on_screen, [self.x - prj_speed, self.y, prj_speed, self.height]):
                    self.x -= prj_speed
                else:
                    for k in range(prj_speed):
                        if not collision(self.platforms_on_screen, [self.x - 1, self.y, 1, self.height]):
                            self.x -= 1
                self.project_speed += 0.2
            else:  # left
                if not collision(self.platforms_on_screen, [self.x + self.width, self.y, prj_speed, self.height]):
                    self.x += prj_speed
                else:
                    for k in range(prj_speed):
                        if not collision(self.platforms_on_screen, [self.x + self.width, self.y, 1, self.height]):
                            self.x += 1
                self.project_speed -= 0.2

        self.change_animation("jump")

        speed = self.jump_speed - self.actual_jump // (self.jump_height // 2)
        if self.jump_height > self.actual_jump + speed and not collision(
                not_names(self.platforms_on_screen, ["bridge"]),
                [self.x, self.y - speed, self.width, speed]):
            self.y -= speed
            self.actual_jump += speed

            # adaptively jump
            if not is_key_pressed("jump"):
                self.actual_jump += speed * 2 // 3
        else:
            for k in range(speed - 1):
                if (self.jump_height <= self.actual_jump or
                        collision(not_names(self.platforms_on_screen, ["bridge"]),
                                  [self.x, self.y - 1, self.width, 1])):
                    self.jumping = False
                    self.actual_jump = 0
                    self.gravity = self.reset_gravity
                    self.possibility_double_jump[0] = True
                    self.project_speed = 0
                    break
                else:
                    self.y -= 1
                    self.actual_jump += 1

    def dash(self):
        self.change_animation("dash")
        x = self.x
        if self.direction == -1:
            x -= 1
        else:
            x += self.width

        out = False
        for k in range(self.dash_speed):
            if self.dying():
                out = True
                break
            elif not collision(self.platforms_on_screen, [x, self.y, 1, self.height]):
                x += self.direction
                self.x += self.direction
                self.actual_dash += 1
            else:
                # destructible
                for destructible in is_names(self.platforms_on_screen, ["destructible"]):
                    if collision([destructible], [x, self.y, 1, self.height]):
                        destructible.destroy()
                        self.x -= self.direction
                        self.destruction = True
                        self.project_speed = self.reset_project_speed * -self.direction
                        break
                out = True
                break

        if self.actual_dash >= self.dash_distance or out:
            self.time_dash = self.reset_time_dash
            self.actual_dash = 0
            self.dashing = False
            self.gravity = self.reset_gravity

    def crouch(self):
        if self.crouching and self.jumping:
            self.num_animation = 0
            self.animation = "jump"
            self.crouching = False
            self.y -= 8
            self.height += 8
        elif (not self.crouching and is_key_pressed("crouch") and self.movement_time >= self.min_movement_time and
              collision(self.platforms_on_screen, [self.x, self.y + self.height, self.width, 1])):  # slide
            self.animation = "slide"
            self.sliding = True
            self.movement_time = 0
            self.y += PYXEL_SIZE // 2
            self.height //= 2
            self.ice_move = self.ice_speed * self.direction

        elif not self.crouching and is_key_pressed("crouch") and collision(self.platforms_on_screen,
                                                                           [self.x, self.y + self.height, self.width,
                                                                            1]):  # activate crouch
            self.crouching = True
            self.y += 8
            self.height -= 8
            self.change_animation("crouch")
        elif self.crouching and not is_key_pressed("crouch") and not collision(self.platforms_on_screen,
                                                                               [self.x, self.y - 8, self.width,
                                                                                1]):  # deactivate crouch
            self.num_animation = 0
            self.animation = "idle"
            self.crouching = False
            self.y -= 8
            self.height += 8

    def slide(self):
        def reset_slide():
            self.sliding = False
            self.num_animation = 0
            self.y -= PYXEL_SIZE // 2
            self.height *= 2
            self.ice_move = (self.slide_speed + 1) * self.direction
            self.slide_speed = self.reset_slide_speed
            self.animation, self.num_animation = "after_slide", 0

        if ((not is_key_pressed("crouch") or self.slide_speed <= 1.5 or
             not collision(self.platforms_on_screen, [self.x, self.y + self.height, self.width, 1]))
                and not collision(self.platforms_on_screen,
                                  [self.x, self.y - PYXEL_SIZE // 2, self.width, self.height])):
            reset_slide()

        elif self.direction == 1:  # right
            slide_speed = int(self.slide_speed)
            if not collision(self.platforms_on_screen, [self.x + self.width, self.y, slide_speed, self.height]):
                self.x += slide_speed
            else:
                for k in range(int(self.slide_speed) - 1):
                    if not collision(self.platforms_on_screen, [self.x + self.width, self.y, 1, self.height]):
                        self.x += 1
                    else:
                        reset_slide()
                        break
        else:  # left
            slide_speed = int(self.slide_speed)
            if not collision(self.platforms_on_screen, [self.x - slide_speed, self.y, slide_speed, self.height]):
                self.x -= slide_speed
            else:
                for k in range(int(self.slide_speed) - 1):
                    if not collision(self.platforms_on_screen, [self.x - 1, self.y, 1, self.height]):
                        self.x -= 1
                    else:
                        reset_slide()
                        break

        if self.slide_speed > 1.5:
            if collision(is_names(self.platforms_on_screen, ["ice"]), [self.x, self.y + self.height, self.width, 1]):
                self.slide_speed -= 0.04
            else:
                self.slide_speed -= 0.06

    def ice_movements(self):
        less = 0.05

        if self.ice_move != 0:
            ice_move = abs(int(self.ice_move))
            if self.ice_move > 0.5:  # right
                if not collision(self.platforms_on_screen, [self.x + self.width, self.y, ice_move, self.height]):
                    self.x += ice_move
                else:
                    for k in range(ice_move - 1):
                        if not collision(self.platforms_on_screen, [self.x + self.width, self.y, 1, self.height]):
                            self.x += 1
                        else:
                            break

                self.ice_move -= less

            elif self.ice_move < -0.5:  # left
                if not collision(self.platforms_on_screen, [self.x - ice_move, self.y, ice_move, self.height]):
                    self.x -= ice_move
                else:
                    for k in range(ice_move - 1):
                        if not collision(self.platforms_on_screen, [self.x - 1, self.y, 1, self.height]):
                            self.x -= 1
                        else:
                            break

                self.ice_move += less

            else:
                self.ice_move = 0

    def move(self, events):
        x, y = self.x, self.y
        self.platforms_on_screen = items_in_screen(self.platforms)

        # dash
        if self.dashing:
            self.dash()
        else:
            if self.sliding:
                self.slide()
            else:
                # start dash
                if self.time_dash == 0:
                    if self.actual_dash_number < self.dash_number:
                        for event in events:
                            if is_key_pressed("dash", event):
                                self.dashing = True
                                self.actual_dash_number += 1
                                if self.jumping:
                                    self.jumping = False
                                    self.possibility_double_jump[0] = True
                                self.actual_jump = 0
                                break
                else:
                    self.time_dash -= 1

                if self.destruction:  # projection after destructing a destructible
                    if self.project_speed < 0:  # right
                        for k in range(-int(self.project_speed)):
                            if not collision(self.platforms_on_screen, [self.x - 1, self.y, 1, self.height]):
                                self.x -= 1
                        self.project_speed += 0.3
                    else:  # left
                        for k in range(int(self.project_speed)):
                            if not collision(self.platforms_on_screen, [self.x + self.width, self.y, 1, self.height]):
                                self.x += 1
                        self.project_speed -= 0.3

                    if -0.4 <= self.project_speed <= 0.4:
                        self.project_speed = 0
                        self.destruction = False
                else:
                    self.crouch()
                    self.left_and_right()

            # jump
            if self.jumping:
                self.jump()
            else:
                # start jump
                if is_key_pressed("jump") and not collision(not_names(self.platforms_on_screen, ["bridge"]),
                                                            [self.x, self.y - 1, self.width, 1]):
                    if collision(self.platforms_on_screen,
                                 [self.x, self.y + self.height, self.width, 1]):  # on the floor
                        if not self.crouching or not collision(not_names(self.platforms_on_screen, ["bridge"]),
                                                               [self.x, self.y - 9, self.width, 1]):
                            self.jumping = True

                            # reset slide
                            if self.sliding:
                                self.sliding = False
                                self.num_animation = 0
                                self.y -= PYXEL_SIZE // 2
                                self.height *= 2
                                self.ice_move = (self.slide_speed + 1) * self.direction
                                self.slide_speed = self.reset_slide_speed
                                self.animation, self.num_animation = "after_slide", 0
                    elif (collision(not_names(self.platforms_on_screen, ["ice", "bridge"]),
                                    [self.x + self.width, self.y, 1, self.height])
                          and self.can_wall_jump[1]):  # wall jump right
                        self.direction = -1
                        self.jumping = True
                        self.can_wall_jump[1] = False
                        self.can_wall_jump[0] = True
                        self.project_speed = -self.reset_project_speed
                    elif (collision(not_names(self.platforms_on_screen, ["ice", "bridge"]),
                                    [self.x - 1, self.y, 1, self.height])
                          and self.can_wall_jump[0]):  # wall jump left
                        self.direction = 1
                        self.jumping = True
                        self.can_wall_jump[0] = False
                        self.can_wall_jump[1] = True
                        self.project_speed = self.reset_project_speed
                    elif (self.gravity >= self.reset_gravity + 1 and self.possibility_double_jump[0] and
                          self.possibility_double_jump[1] < self.double_jump_number):  # in the air
                        for event in events:
                            if is_key_pressed("jump", event):
                                self.jumping = True
                                self.possibility_double_jump = [False, self.possibility_double_jump[1] + 1]
                                break

                # gravity
                collide_bridge = collision(is_names(self.platforms_on_screen, ["bridge"]),
                                           [self.x, self.y + self.height - 1, self.width, 1])
                if not collision(self.platforms_on_screen,
                                 [self.x, self.y + self.height, self.width, 1]) or collide_bridge:
                    self.change_animation("fall")
                    self.y += 1
                    for k in range(int(self.gravity)):
                        if not collision(self.platforms_on_screen,
                                         [self.x, self.y + self.height, self.width, 1]) or collide_bridge:
                            self.y += 1
                            # charge slide in midair
                            if is_key_pressed("left") or is_key_pressed("right"):
                                if self.movement_time < self.min_movement_time:
                                    self.movement_time += 0.3
                            else:
                                self.movement_time = 0

                    if self.gravity <= 9.6:
                        self.gravity += 0.4
                else:
                    if ((is_key_pressed("left") or is_key_pressed("right")) and not
                    (x, y) == (self.x, self.y) and not self.destruction):
                        self.change_animation("run")
                    self.gravity = self.reset_gravity
                    self.possibility_double_jump = [False, 0]
                    self.actual_dash_number = 0
                    self.can_wall_jump = [True, True]

        if (x, y) == (self.x, self.y):  # not moving
            if self.animation != "idle":
                self.change_animation("idle")
                self.movement_time = 0
        else:
            # invisible platforms
            self.vision = False
            if self.activate_eye is not None:
                self.activate_eye.activate = False

        # stop the eye effect if the eye is out of the screen or if it's deactivate
        if self.activate_eye is not None:
            if items_in_screen([self.activate_eye]) == [] or self.activate_eye.animation == 0:
                self.activate_eye = None

        if abs(self.ice_move) > 0 and (self.crouching or not collision(is_names(self.platforms_on_screen, ["ice"]),
                                                                       [self.x, self.y + self.height, self.width, 1])):
            self.ice_move = 0
        elif (x, y) == (self.x, self.y) or self.sliding:
            self.ice_movements()

        # animation
        self.num_animation += ANIMATION_TIME[self.animation]
        if self.num_animation >= len(PLAYER_ANIMATIONS[self.animation]):
            self.num_animation = 0

    def update(self, events):
        self.move(events)

        # invisible
        self.get_vision(events)

    def display(self, screen):
        # display player
        screen.blit(get_image(self.sprite_sheet, PLAYER_ANIMATIONS[self.animation][int(self.num_animation)][0],
                              PLAYER_ANIMATIONS[self.animation][int(self.num_animation)][1], self.width + 8,
                              self.height,
                              colour=BLACK, reverse=(self.direction == -1)), (self.x - 4, self.y))

        # display collision
        # pg.draw.rect(screen, RED, pg.Rect(self.x, self.y, self.width, self.height), 1)
