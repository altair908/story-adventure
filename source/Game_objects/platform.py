import pygame as pg

from parameters import *


"""def not_moving(platforms: list) -> list:
    return [p for p in platforms if p.move == 0]"""

"""    def init_images(self, y1=0):
        if self.name in ["destructible", "bridge", "red", "blue"] or self.move != 0:
            self.init_specific_images(y1)
            return

        self.images = []
        if self.name in ["bridge"]:
            self.images = [[get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE // 2, PYXEL_SIZE // 2, colour=GREEN), (k, 0)]
                           for k in range(0, self.width, PYXEL_SIZE // 2)]
        else:
            angles = [get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE, PYXEL_SIZE, angle=k*-90, colour=GREEN)
                      for k in range(4)]
            half_angles1 = [get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE, PYXEL_SIZE // 2, angle=k * -90, colour=GREEN)
                            for k in range(4)]
            half_angles2 = [get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE // 2, PYXEL_SIZE, angle=k * -90, colour=GREEN)
                            for k in range(4)]
            quart_angles = [get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE // 2, PYXEL_SIZE // 2, angle=k * -90, colour=GREEN)
                            for k in range(4)]
            borders = [get_image(self.sprite_sheet, PYXEL_SIZE, y1, PYXEL_SIZE, PYXEL_SIZE, angle=k*-90, colour=GREEN)
                       for k in range(4)]
            half_borders = [get_image(self.sprite_sheet, PYXEL_SIZE, y1, PYXEL_SIZE, PYXEL_SIZE // 2, angle=k*-90,
                                      colour=GREEN) for k in range(4)]
            center = get_image(self.sprite_sheet, PYXEL_SIZE * 2, y1, PYXEL_SIZE, PYXEL_SIZE, colour=GREEN)

            # perfect size (proportion by PYXEL_SIZE)
            perfect_width, perfect_height = self.width % PYXEL_SIZE == 0, self.height % PYXEL_SIZE == 0
            perfect = perfect_width and perfect_height

            platforms = is_names(not_moving(self.platforms), [self.name])

            for x in range(self.x, self.x + self.width, PYXEL_SIZE):
                for y in range(self.y, self.y + self.height, PYXEL_SIZE):
                    coord = (x - self.x, y - self.y)
                    if not perfect and (x + PYXEL_SIZE > self.width or y + PYXEL_SIZE > self.height):
                        if not perfect_width:
                            right, left = (collision(platforms, [x + PYXEL_SIZE // 2, y + PYXEL_SIZE // 2 - 1, 1, 2]),
                                                 collision(platforms, [x - 1, y + PYXEL_SIZE // 2 - 1, 1, 2]))
                        else:
                            right, left = (collision(platforms, [x + PYXEL_SIZE, y + PYXEL_SIZE // 2 - 1, 1, 2]),
                                                 collision(platforms, [x - 1, y + PYXEL_SIZE // 2 - 1, 1, 2]))

                        if not perfect_height:
                            up, down = (collision(platforms, [x + PYXEL_SIZE // 2 - 1, y - 1, 2, 1]),
                                        collision(platforms, [x + PYXEL_SIZE // 2 - 1, y + PYXEL_SIZE // 2, 2, 1]))
                        else:
                            up, down = (collision(platforms, [x + PYXEL_SIZE // 2 - 1, y - 1, 2, 1]),
                                        collision(platforms, [x + PYXEL_SIZE // 2 - 1, y + PYXEL_SIZE, 2, 1]))

                    else:
                        right, left, up, down = (collision(platforms, [x + PYXEL_SIZE, y + PYXEL_SIZE // 2 - 1, 1, 2]),
                                                 collision(platforms, [x - 1, y + PYXEL_SIZE // 2 - 1, 1, 2]),
                                                 collision(platforms, [x + PYXEL_SIZE // 2 - 1, y - 1, 2, 1]),
                                                 collision(platforms, [x + PYXEL_SIZE // 2 - 1, y + PYXEL_SIZE, 2, 1]))

                    num_coll = 0
                    for b in [right, left, up, down]:
                        if b: num_coll += 1

                    # look collisions around
                    # center
                    if num_coll == 4:
                        self.images.append([center, coord])

                    # border on big blocks
                    elif num_coll == 3:
                        if left and down and right:
                            self.images.append([borders[0], coord])
                        elif up and left and down:
                            if perfect_width:
                                self.images.append([borders[1], coord])
                            else:
                                self.images.append([half_borders[1], coord])
                        elif left and up and right:
                            if perfect_height:
                                self.images.append([borders[2], coord])
                            else:
                                self.images.append([half_borders[2], coord])
                        else:
                            self.images.append([borders[3], coord])

                    # angle or small border
                    elif num_coll == 2:
                        # angle
                        if right and down:
                            self.images.append([angles[0], coord])
                        elif left and down:
                            if perfect_width:
                                self.images.append([angles[1], coord])
                            else:
                                self.images.append([half_angles1[1], coord])
                        elif left and up:
                            if perfect:
                                self.images.append([angles[2], coord])
                            elif perfect_width:
                                self.images.append([half_angles1[2], coord])
                            elif perfect_height:
                                self.images.append([half_angles2[2], coord])
                            else:
                                self.images.append([quart_angles[2], coord])
                        elif up and right:
                            if perfect_height:
                                self.images.append([angles[3], coord])
                            else:
                                self.images.append([half_angles2[3], coord])

                        # small border
                        elif left and right:
                            self.images.append([half_borders[0], coord])
                            self.images.append([half_borders[2], (coord[0], coord[1] + PYXEL_SIZE // 2)])

                        else:
                            self.images.append([half_borders[3], coord])
                            self.images.append([half_borders[1], (coord[0] + PYXEL_SIZE // 2, coord[1])])

                    elif num_coll == 1:
                        if left:
                            if perfect_width:
                                self.images.append([half_angles2[1], coord])
                                self.images.append([half_angles1[2], (coord[0], coord[1] + PYXEL_SIZE // 2)])
                            else:
                                self.images.append([half_angles2[1], (coord[0] - PYXEL_SIZE // 2, coord[1])])
                                self.images.append([half_angles1[2], (coord[0] - PYXEL_SIZE // 2, coord[1] + PYXEL_SIZE // 2)])
                        elif right:
                            self.images.append([half_angles1[0], coord])
                            self.images.append([half_angles2[3], (coord[0], coord[1] + PYXEL_SIZE // 2)])
                        elif down:
                            self.images.append([half_angles2[0], coord])
                            self.images.append([half_angles1[1], (coord[0] + PYXEL_SIZE // 2, coord[1])])
                        else:
                            if perfect_height:
                                self.images.append([half_angles1[3], coord])
                                self.images.append([half_angles2[2], (coord[0] + PYXEL_SIZE // 2, coord[1])])
                            else:
                                self.images.append([half_angles1[3], (coord[0], coord[1] - PYXEL_SIZE // 2)])
                                self.images.append([half_angles2[2], (coord[0] + PYXEL_SIZE // 2, coord[1] - PYXEL_SIZE // 2)])

                    else:
                        # simple square
                        angles = [0, -90, 90, -180]
                        for k in range(4):
                            self.images.append([get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE // 2, PYXEL_SIZE // 2,
                                                          angle=angles[k], colour=GREEN),
                                                ((k % 2) * (PYXEL_SIZE // 2), (k // 2) * (PYXEL_SIZE // 2))])"""


class Platform:
    def __init__(self, name, x, y, width, height, world, player, move=0):
        self.name = name
        self.x, self.y, self.width, self.height = x, y, width, height

        # self.world = world
        self.doors = world[0]
        self.platforms = world[1]
        self.spikes = world[2]
        self.signs = world[4]

        self.player = player

        self.sprite_sheet = pg.image.load(f"assets/Game_objects/platform/{self.name}.png")

        self.move = abs(move)
        self.direction = 1  # right / left
        if self.move != 0:  # moving
            if move < 0:  # up / down
                self.direction = -1

            self.actual_move = 0
            self.speed = 1

            # select spikes around
            self.around_spikes = None

        # destructible
        if self.name == "destructible":
            self.destruction = False
            self.max_animation = self.sprite_sheet.get_size()[1] // PYXEL_SIZE - 0.25
            self.num_animation = 0

            # select spikes around
            self.around_spikes = None

        # red and blue (change fase)
        elif self.name in ["red", "blue"]:
            self.activate = (self.name == "blue")  # first will be red
            self.timer = 10000
            self.time_activate = 3  # in seconds

        # invisible
        elif self.name == "invisible":
            self.displaying = True

            # select spikes around
            self.around_spikes = None

        self.images = []  # [[img, (x, y)], [...], ...]
        self.init_img = False

    def init_images(self, y1=0):
        if self.name in ["bridge"]:
            self.images = [[get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE // 2, PYXEL_SIZE // 2, colour=GREEN), (k, 0)] for k in range(0, self.width, PYXEL_SIZE // 2)]
        else:
            self.images = []
            if (self.width, self.height) == (PYXEL_SIZE, PYXEL_SIZE):
                # simple square
                angles = [0, -90, 90, -180]
                for k in range(4):
                    self.images.append([get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE // 2, PYXEL_SIZE // 2,
                                                  angle=angles[k], colour=GREEN),
                                        ((k % 2) * (PYXEL_SIZE // 2), (k // 2) * (PYXEL_SIZE // 2))])
            else:
                angles = [get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE, PYXEL_SIZE, angle=k * -90, colour=GREEN)
                          for k in range(4)]
                half_angles1 = [
                    get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE, PYXEL_SIZE // 2, angle=k * -90, colour=GREEN)
                    for k in range(4)]
                half_angles2 = [
                    get_image(self.sprite_sheet, 0, y1, PYXEL_SIZE // 2, PYXEL_SIZE, angle=k * -90, colour=GREEN)
                    for k in range(4)]
                borders = [
                    get_image(self.sprite_sheet, PYXEL_SIZE, y1, PYXEL_SIZE, PYXEL_SIZE, angle=k * -90, colour=GREEN)
                    for k in range(4)]
                half_borders = [get_image(self.sprite_sheet, PYXEL_SIZE, y1, PYXEL_SIZE, PYXEL_SIZE // 2, angle=k * -90,
                                          colour=GREEN) for k in range(4)]
                center = get_image(self.sprite_sheet, PYXEL_SIZE * 2, y1, PYXEL_SIZE, PYXEL_SIZE, colour=GREEN)

                for x in range(self.x, self.x + self.width, PYXEL_SIZE):
                    for y in range(self.y, self.y + self.height, PYXEL_SIZE):
                        if self.width == PYXEL_SIZE:  # vertical
                            if y == self.y:  # top
                                self.images.append([half_angles2[0], (x - self.x, y - self.y)])
                                self.images.append([half_angles1[1], (x - self.x + PYXEL_SIZE // 2, y - self.y)])
                            elif y >= self.y + self.height - PYXEL_SIZE:  # bottom
                                self.images.append([half_angles1[3], (x - self.x, self.height - PYXEL_SIZE)])
                                self.images.append([half_angles2[2], (x - self.x + PYXEL_SIZE // 2, self.height - PYXEL_SIZE)])
                            else:  # center
                                self.images.append([half_borders[3], (x - self.x, y - self.y)])
                                self.images.append([half_borders[1], (x - self.x + PYXEL_SIZE // 2, y - self.y)])
                        elif self.height == PYXEL_SIZE:  # horizontal
                            if x == self.x:  # left
                                self.images.append([half_angles1[0], (x - self.x, y - self.y)])
                                self.images.append([half_angles2[3], (x - self.x, y - self.y + PYXEL_SIZE // 2)])
                            elif x >= self.x + self.width - PYXEL_SIZE:  # right
                                self.images.append([half_angles2[1], (self.width - PYXEL_SIZE, y - self.y)])
                                self.images.append([half_angles1[2],
                                                    (self.width - PYXEL_SIZE, y - self.y + PYXEL_SIZE // 2)])
                            else:  # center
                                self.images.append([half_borders[0], (x - self.x, y - self.y)])
                                self.images.append([half_borders[2], (x - self.x, y - self.y + PYXEL_SIZE // 2)])
                        elif x == self.x:  # left
                            if y == self.y:  # top corner
                                self.images.append([angles[0], (x - self.x, y - self.y)])
                            elif y >= self.y + self.height - PYXEL_SIZE:  # bottom corner
                                self.images.append([angles[3], (x - self.x, self.height - PYXEL_SIZE)])
                            else:
                                self.images.append([borders[3], (x - self.x, y - self.y)])
                        elif x >= self.x + self.width - PYXEL_SIZE:  # right
                            if y == self.y:  # top corner
                                self.images.append([angles[1], (self.width - PYXEL_SIZE, y - self.y)])
                            elif y >= self.y + self.height - PYXEL_SIZE:  # bottom corner
                                self.images.append([angles[2], (self.width - PYXEL_SIZE, self.height - PYXEL_SIZE)])
                            else:
                                self.images.append([borders[1], (self.width - PYXEL_SIZE, y - self.y)])

                        elif y == self.y:  # top
                            self.images.append([borders[0], (x - self.x, y - self.y)])

                        elif y >= self.y + self.height - PYXEL_SIZE:  # bottom
                            self.images.append([borders[2], (x - self.x, self.height - PYXEL_SIZE)])

                        else:  # center
                            self.images.append([center, (x - self.x, y - self.y)])

    def movements(self, platforms_on_screen):
        on_screen = platforms_on_screen != []

        if self.around_spikes is None:
            self.around_spikes = [spike for spike in self.spikes if
                                  (collision([spike], [self.x - 1, self.y, 1, self.height]) and spike.width == PYXEL_SIZE // 2)
                                  or (collision([spike], [self.x + self.width, self.y, 1, self.height]) and spike.width == PYXEL_SIZE // 2)
                                  or (collision([spike], [self.x, self.y - 1, self.width, 1]) and spike.height == PYXEL_SIZE // 2)
                                  or (collision([spike], [self.x, self.y + self.height, self.width, 1]) and spike.height == PYXEL_SIZE // 2)]

        if self.direction == 1:  # right or left
            if self.speed > 0:  # right
                for k in range(self.speed):
                    if self.actual_move >= self.move:
                        self.actual_move = 0
                        self.speed *= -1
                        break
                    else:
                        self.x += 1
                        self.actual_move += 1
                        # move around spikes
                        for spike in self.around_spikes:
                            spike.x += 1

                        if on_screen:
                            # move player
                            if collision([self], [self.player.x, self.player.y + self.player.height, self.player.width, 1], True) and \
                                    not collision(platforms_on_screen, [self.player.x + self.player.width, self.player.y, 1,
                                                                   self.player.height]):
                                self.player.x += 1

                            # player at right side of the platform
                            elif collision([self], [self.player.x - 1, self.player.y, 1, self.player.width], True):
                                # player cant move
                                if collision(platforms_on_screen, [self.player.x + self.player.width, self.player.y, 1, self.player.height]):
                                    self.player.die = True
                                    break
                                else:
                                    self.player.x += 1
            else:  # left
                for k in range(-self.speed):
                    if self.actual_move >= self.move:
                        self.actual_move = 0
                        self.speed *= -1
                        break
                    else:
                        self.x -= 1
                        self.actual_move += 1
                        # move around spikes
                        for spike in self.around_spikes:
                            spike.x -= 1

                        if on_screen:
                            # move player
                            if collision([self], [self.player.x, self.player.y + self.player.height, self.player.width, 1], True) and \
                                    not collision(platforms_on_screen, [self.player.x - 1, self.player.y, 1,
                                                                   self.player.height]):
                                self.player.x -= 1

                            # player at left side of the platform
                            elif collision([self.player], [self.x - 1, self.y, 1, self.height], True):
                                # player cant move
                                if collision(platforms_on_screen,
                                             [self.player.x - 1, self.player.y, 1, self.player.height]):
                                    self.player.die = True
                                    break
                                else:
                                    self.player.x -= 1
        else:  # down or up
            if self.speed > 0:  # down
                for k in range(self.speed):
                    if self.actual_move >= self.move:
                        self.actual_move = 0
                        self.speed *= -1
                        break
                    else:
                        self.y += 1
                        self.actual_move += 1
                        # move around spikes
                        for spike in self.around_spikes:
                            spike.y += 1

                        if on_screen:
                            # move player
                            if collision([self], [self.player.x, self.player.y + self.player.height + 2, self.player.width, 1], True):
                                self.player.y += 1

                            # player under the platform
                            if collision([self], [self.player.x, self.player.y - 1, self.player.width, 1], True):
                                # player cant move
                                if collision(platforms_on_screen,
                                             [self.player.x, self.player.y + self.player.height, self.player.width, 1]):
                                    self.player.die = True
                                    break
                                else:
                                    self.player.y += 1
            else:  # up
                for k in range(-self.speed):
                    if self.actual_move >= self.move:
                        self.actual_move = 0
                        self.speed *= -1
                        break
                    else:
                        self.y -= 1
                        self.actual_move += 1
                        # move around spikes
                        for spike in self.around_spikes:
                            spike.y -= 1

                        if on_screen:
                            # player on top of the platform
                            if collision([self], [self.player.x, self.player.y + self.player.height, self.player.width, 1], True):
                                # player cant move
                                if collision(platforms_on_screen, [self.player.x, self.player.y - 1, self.player.width, 1]):
                                    self.player.die = True
                                    break
                                else:
                                    self.player.y -= 1

    def destroy(self):
        self.destruction = True
        if self.num_animation == 0:
            for spike in [spike for spike in self.spikes if collision([spike], [self.x - 1, self.y - 1, self.width + 2, self.height + 2], False)]:
                self.spikes.remove(spike)

        # animation
        if self.num_animation >= self.max_animation:
            # destroy
            self.platforms.remove(self)
        else:
            self.num_animation += 0.25
            self.init_images(int(self.num_animation) * PYXEL_SIZE)

    def red_blue(self):
        self.timer += 1

        if self.timer >= FPS * self.time_activate:
            if self.activate:
                self.activate = False
                self.init_images(PYXEL_SIZE)
            else:
                self.activate = True
                self.init_images()

            self.timer = 0

    def invisible(self):
        # eye not close
        self.displaying = self.player.activate_eye is not None or self.player.vision

        if self.around_spikes is None:
            self.around_spikes = []
            for spike in self.spikes:
                if ((collision([spike], [self.x - 1, self.y, 1, self.height]) and spike.width == PYXEL_SIZE // 2)
                        or (collision([spike], [self.x + self.width, self.y, 1, self.height]) and spike.width == PYXEL_SIZE // 2)
                        or (collision([spike], [self.x, self.y - 1, self.width, 1]) and spike.height == PYXEL_SIZE // 2)
                        or (collision([spike], [self.x, self.y + self.height, self.width, 1]) and spike.height == PYXEL_SIZE // 2)):
                    spike.displaying = self.displaying
                    self.around_spikes.append(spike)

        # manage around spikes
        if self.around_spikes != [] and self.around_spikes[0].displaying != self.displaying:
            for spike in self.around_spikes:
                spike.displaying = self.displaying

    def update(self):
        if not self.init_img:
            self.init_images()
            self.init_img = True

        platforms_on_screen = items_in_screen(self.platforms)

        # moving platform
        if self.move != 0:
            self.movements(platforms_on_screen)

        # destructible
        if self.name == "destructible" and self.destruction:
            self.destroy()

        # red or blue
        if self.name in ["red", "blue"]:
            self.red_blue()

        # invisible
        if self.name == "invisible":
            self.invisible()

    def display(self, screen):
        if self.name in ["red", "blue"]:
            text = get_text(f"{self.time_activate - self.timer // FPS}", TEXT_SIZE, WHITE)
            text_rect = text.get_rect()
            if self.images != []:
                screen.blit(text, (self.images[0][1][0] + self.x + self.width // 2 - text_rect.width // 2, self.images[0][1][1] + self.y + self.height // 2 - text_rect.height // 2))

        if self.name != "invisible":
            for img in self.images:
                screen.blit(img[0], (img[1][0] + self.x, img[1][1] + self.y))
        elif self.displaying:
            for img in self.images:
                screen.blit(img[0], (img[1][0] + self.x, img[1][1] + self.y))

        # display collision
        # pg.draw.rect(screen, RED, pg.Rect(self.x, self.y, self.width, self.height), 1)
