import pygame as pg

from parameters import *


class Camera:
    def __init__(self, player, world, spawn_point=None):
        self.x, self.y, self.width, self.height = 0, 0, SCREEN_SIZE[0], SCREEN_SIZE[1]
        self.player = player
        self.world = world

        self.spawn_point = spawn_point

    def reset(self):
        move_x, move_y = -self.x, -self.y
        for items in self.world:
            for item in items:
                item.x += move_x
                item.y += move_y
        if self.spawn_point is not None:
            self.spawn_point.x += move_x
            self.spawn_point.y += move_y
        self.player.x += move_x
        self.player.y += move_y
        self.x, self.y = 0, 0

    def move_left(self, pxl: int):
        for items in self.world:
            for item in items:
                item.x += pxl
        if self.spawn_point is not None:
            self.spawn_point.x += pxl
        self.player.x += pxl
        self.x += pxl

    def move_right(self, pxl: int):
        for items in self.world:
            for item in items:
                item.x -= pxl
        if self.spawn_point is not None:
            self.spawn_point.x -= pxl
        self.player.x -= pxl
        self.x -= pxl

    def move_up(self, pxl: int):
        for items in self.world:
            for item in items:
                item.y += pxl
        if self.spawn_point is not None:
            self.spawn_point.y += pxl
        self.player.y += pxl
        self.y += pxl

    def move_down(self, pxl: int):
        for items in self.world:
            for item in items:
                item.y -= pxl
        if self.spawn_point is not None:
            self.spawn_point.y -= pxl
        self.player.y -= pxl
        self.y -= pxl

    def update(self, x, y):
        move_x, move_y = -x + self.width // 2, -y + self.height // 2
        for items in self.world:
            for item in items:
                item.x += move_x
                item.y += move_y
        if self.spawn_point is not None:
            self.spawn_point.x += move_x
            self.spawn_point.y += move_y
        self.player.x += move_x
        self.player.y += move_y
        self.x, self.y = self.x + move_x, self.y + move_y
