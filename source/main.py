import pygame as pg
import time
 
from parameters import *
from the_ancients import TheAncients


class Cursor:
    def __init__(self):
        self.x, self.y = 0, 0
        self.img = pg.image.load("assets/Menu/Cursors/cursor.png")
        self.img.set_colorkey(GREEN)

    def update(self):
        self.x, self.y = pg.mouse.get_pos()
        if self.x >= SCREEN_SIZE[0]:
            self.x = SCREEN_SIZE[0] - 2
            pg.mouse.set_pos(self.x, self.y)
        if self.y >= SCREEN_SIZE[1]:
            self.y = SCREEN_SIZE[1] - 2
            pg.mouse.set_pos(self.x, self.y)

    def display(self, screen):
        screen.blit(self.img, (self.x, self.y))


class Button:
    def __init__(self, text, x, y, width, height, cursor, key=None):
        self.text = text
        self.x, self.y, self.width, self.height = x, y, width, height

        # select keys
        self.key = key

        self.cursor = cursor

        self.img = pg.image.load("assets/Menu/Buttons/btn.png")

    def press(self, events) -> bool:
        for event in events:
            if event.type == pg.MOUSEBUTTONDOWN:
                if event.button == 1:  # left clic
                    return self.x <= self.cursor.x <= self.x + self.width and self.y <= self.cursor.y <= self.y + self.height
                return False
        return False

    def update(self): pass

    def display(self, screen, txt=None):
        if txt is None:
            txt = self.key

        # text before button
        if txt is not None:
            text = get_text(self.text + " : ", TEXT_SIZE, WHITE)
            text_rect = text.get_rect()
            screen.blit(text, (self.x - text_rect.width - 4, self.y + self.height // 2 - text_rect.height // 2))

        # button
        screen.blit(get_image(self.img, 0, 0, self.width, self.height, colour=GREEN), (self.x, self.y))

        # text in button
        if txt is not None:
            text = get_text(txt, TEXT_SIZE, WHITE)
        else:
            text = get_text(self.text, TEXT_SIZE, WHITE)
        text_rect = text.get_rect()
        screen.blit(text, (self.x + self.width // 2 - text_rect.width // 2, self.y + self.height // 2 - text_rect.height // 2))


class App:
    def __init__(self):
        # PYGAME INITIALISATION
        pg.init()

        # music
        pg.mixer.init(frequency=44100, buffer=512)
        pg.mixer.music.load("assets/Musics/music.mp3")
        pg.mixer.music.set_volume(0.3)
        pg.mixer.music.play(loops=-1)

        # window
        pg.display.set_caption("Platformer")
        pg.display.set_icon(pg.image.load("assets/Icon/icon.png"))
        self.window = pg.display.set_mode(SCREEN_SIZE, pg.RESIZABLE)

        # screen size
        self.screen = pg.Surface(SCREEN_SIZE)
        self.spawn = (0, 0)
        self.scale_factor = 1
        self.window_size, self.screen_size = SCREEN_SIZE, SCREEN_SIZE

        # cursor
        pg.mouse.set_visible(False)
        self.cursor = Cursor()

        # fps
        self.clock = pg.time.Clock()

        # menu
        self.in_game = False
        self.in_parameters = False
        self.buttons = None
        self.button_select = None  # parameters
        self.init_buttons()

        # game
        self.game = TheAncients(self.screen)

        # game time
        self.start_time = 0

        self.running = True
        self.main()

    def init_buttons(self):
        if self.in_game:  # game
            self.buttons = None
            self.cursor = None
        elif self.in_parameters:  # parameters
            width, height = PYXEL_SIZE * 4, PYXEL_SIZE
            x, y = PYXEL_SIZE + PYXEL_SIZE // 2, PYXEL_SIZE + 4
            space = PYXEL_SIZE
            self.buttons = []

            names = [name for name in KEYS.keys()]
            keys = [val for val in KEYS.values()]
            i_change = 0
            for i in range(len(names)):
                if y + (i + 1) * height + i * space >= SCREEN_SIZE[1]:  # under the screen
                    x = SCREEN_SIZE[0] // 2
                    if i_change == 0:
                        i_change = i
                self.buttons.append(Button(names[i], x + get_text(names[i] + " : ", TEXT_SIZE, WHITE).get_width(), y + (i - i_change) * height + (i - i_change) * space, width, height, self.cursor, keys[i]))

            self.buttons.append(Button("Back", SCREEN_SIZE[0] - PYXEL_SIZE // 2 - width, SCREEN_SIZE[1] - PYXEL_SIZE // 2 - height, width, height, self.cursor))
        else:  # menu
            self.cursor = Cursor()
            width, height = PYXEL_SIZE * 4, PYXEL_SIZE
            x, y = SCREEN_SIZE[0] // 2 - width // 2, SCREEN_SIZE[1] // 2 - height // 2 + 4
            self.buttons = [Button("Play", x, y, width, height, self.cursor),
                            Button("New Game", x, y + PYXEL_SIZE + PYXEL_SIZE // 2, width, height, self.cursor),
                            Button("Parameters", x, y + PYXEL_SIZE * 3, width, height, self.cursor),
                            Button("Exit", x, y + PYXEL_SIZE * 4 + PYXEL_SIZE // 2, width, height, self.cursor)]

    def update(self, events):
        if self.in_game:
            self.game.update(events)
        else:  # menu
            if self.in_parameters:
                if self.button_select is not None:
                    self.button_select.img = pg.image.load("assets/Menu/Buttons/btn_press.png")

                    for event in events:
                        if event.type == pg.KEYDOWN:
                            self.button_select.key = pg.key.name(event.key)
                            KEYS[self.button_select.text] = self.button_select.key
                            write_keys(KEYS)
                            self.button_select.img = pg.image.load("assets/Menu/Buttons/btn.png")
                            self.button_select = None
                            break
                        elif event.type == pg.MOUSEBUTTONDOWN:
                            match event.button:
                                case 1:
                                    name = "mouse left"
                                case 2:
                                    name = "mouse wheel"
                                case 3:
                                    name = "mouse right"
                                case _:
                                    self.button_select.img = pg.image.load("assets/Menu/Buttons/btn.png")
                                    self.button_select = None
                                    break
                            self.button_select.key = name
                            KEYS[self.button_select.text] = self.button_select.key
                            write_keys(KEYS)
                            self.button_select.img = pg.image.load("assets/Menu/Buttons/btn.png")
                            self.button_select = None
                            break

            self.cursor.update()

            if self.start_time != 0:  # save when it's on the menu
                self.game.timer += int(time.time() - self.start_time)
                write_save({"level": self.game.level, "death": self.game.death, "timer": self.game.timer})
                self.start_time = 0

            for btn in self.buttons:
                btn.update()

                if btn.press(events):  # press a button
                    if self.in_parameters:  # parameters
                        if btn.text == "Back":
                            self.in_parameters = False
                            self.init_buttons()
                        else:
                            self.button_select = btn
                    else:  # menu
                        if btn.text == "Play":
                            self.in_game = True
                            self.start_time = time.time()

                            # bug on sign displaying keys
                            for sign in self.game.world[4]:
                                sign.create_text()
                        elif btn.text == "New Game":
                            self.game = TheAncients(self.screen, False)
                            write_save({"level": 1, "death": 0, "timer": 0})
                            self.start_time = time.time()

                            self.in_game = True
                        elif btn.text == "Parameters":
                            self.in_parameters = True
                        else:  # exit
                            self.running = False
                        self.init_buttons()

    def display(self):
        if self.in_game:
            self.game.display()
        else:  # menu
            # background
            img = pg.image.load("assets/Menu/Background/bg.png")
            img_rect = img.get_rect()
            self.screen.blit(img, (0, (SCREEN_SIZE[1] - img_rect.height) // 2))

            if not self.in_game and not self.in_parameters:  # in menu
                text = get_text("The Ancients", TEXT_SIZE * 4, WHITE)
                text_rect = text.get_rect()
                self.screen.blit(text, (SCREEN_SIZE[0] // 2 - text_rect.width // 2, self.buttons[0].y // 2 - text_rect.height // 2))

                # display stats
                self.screen.blit(
                    get_text(f"timer: {self.game.timer // 3600}h {self.game.timer // 60}m {self.game.timer % 60}s",
                             TEXT_SIZE, WHITE), (SCREEN_SIZE[0] // 3 * 2, self.buttons[0].y))
                self.screen.blit(get_text(f"level: {self.game.level}     death: {self.game.death}",
                                          TEXT_SIZE, WHITE), (SCREEN_SIZE[0] // 3 * 2, self.buttons[0].y + TEXT_SIZE + 2))

            for btn in self.buttons:
                if btn is self.button_select:
                    btn.display(self.screen, txt="...")
                else:
                    btn.display(self.screen)

            self.cursor.display(self.screen)

    def main(self):
        while self.running:
            events = pg.event.get()
            for event in events:
                if event.type == pg.QUIT:
                    self.running = False
                elif event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
                    if self.in_game or self.in_parameters:
                        self.in_game, self.in_parameters = False, False
                        self.init_buttons()
                    else:
                        self.running = False
                elif event.type == pg.VIDEORESIZE:
                    self.window_size = event.w, event.h
                    self.window = pg.display.set_mode(self.window_size, pg.RESIZABLE)
                    self.scale_factor = min(self.window_size[0] / SCREEN_SIZE[0],
                                            self.window_size[1] / SCREEN_SIZE[1])
                    self.screen_size = int(SCREEN_SIZE[0] * self.scale_factor), int(SCREEN_SIZE[1] * self.scale_factor)
                    self.spawn = ((self.window_size[0] - self.screen_size[0]) // 2,
                                  (self.window_size[1] - self.screen_size[1]) // 2)

            self.screen.fill(BLACK)

            self.update(events)
            self.display()

            final_screen = pg.transform.scale(self.screen, self.screen_size)
            self.window.fill(BLACK)
            self.window.blit(final_screen, self.spawn)

            pg.display.flip()
            self.clock.tick(FPS)

        pg.quit()


App()
