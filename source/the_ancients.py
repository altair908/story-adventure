import pygame as pg
from time import sleep

from parameters import *
from Game_objects.camera import Camera
from Game_objects.player import Player
from Game_objects.platform import Platform
from Game_objects.spike import Spike
from Game_objects.door import Door
from Game_objects.decor import Decor
from Game_objects.sign import Sign


class TheAncients:
    def __init__(self, screen, save=True):
        # screen size
        self.screen = screen

        if save:
            save = get_save()
            self.level = save["level"]
            self.death = save["death"]
            self.timer = save["timer"]
        else:
            self.level = 1
            self.death = 0
            self.timer = 0

        # Global
        self.decors = []
        self.platforms = []
        self.spikes = []
        self.doors = []
        self.signs = []
        self.world = [self.decors, self.platforms, self.spikes, self.doors, self.signs]
        self.player, self.camera = None, None

        self.init_world()

        # camera
        self.gap_camera = PYXEL_SIZE * 2

        # loading screen
        self.loading = True
        self.frames = 0

    def init_world(self):
        self.loading = True

        # init variables
        self.decors = []
        self.platforms = []
        self.spikes = []
        self.doors = []
        self.signs = []
        self.world = [self.decors, self.platforms, self.spikes, self.doors, self.signs]

        world, spawn_point = get_level(self.level)

        # init player
        self.player = Player(int(spawn_point[0]), int(spawn_point[1]), self.world, self.level)
        self.camera = Camera(self.player, self.world)

        # init objects
        for item in world:
            if item[0] == "Decor":
                self.decors.append(Decor(item[1], int(item[2]), int(item[3]), int(item[4]), int(item[5])))
            elif item[0] == "Platform":
                self.platforms.append(Platform(item[1], int(item[2]), int(item[3]), int(item[4]), int(item[5]), self.world, self.player, int(item[6])))
            elif item[0] == "Spike":
                self.spikes.append(Spike(item[1], int(item[2]), int(item[3]), int(item[4]), int(item[5]), self.world))
            elif item[0] == "Door":
                self.doors.append(Door(item[1], int(item[2]), int(item[3]), self.player))
            elif item[0] == "Sign":
                self.signs.append(Sign(item[1], int(item[2]), int(item[3]), int(item[4]), int(item[5]), self.player, item[6]))

        # decor optimisation
        for decor in self.decors:
            if decor.background:
                tab_remove = []
                for img in decor.images:
                    platforms = [platform for platform in not_names(self.platforms, ["invisible"]) if platform.move == 0]
                    if (collision(platforms, [img[1][0] + decor.x, img[1][1] + decor.y, 1, 1], False)
                            and collision(platforms, [img[1][0] + decor.x + img[0].get_width(), img[1][1] + decor.y, 1, 1], False)
                            and collision(platforms, [img[1][0] + decor.x, img[1][1] + decor.y + img[0].get_height(), 1, 1], False)
                            and collision(platforms, [img[1][0] + decor.x + img[0].get_width(), img[1][1] + decor.y + img[0].get_height(), 1, 1], False)):
                        tab_remove.append(img)
                for img in tab_remove:
                    decor.images.remove(img)

    def update(self, events):
        # camera
        x, y = self.player.x + self.player.width // 2, self.player.y + self.player.height // 2
        if x < SCREEN_SIZE[0] // 2 - self.gap_camera:  # left
            self.camera.move_left(SCREEN_SIZE[0] // 2 - self.gap_camera - x)
        elif x > SCREEN_SIZE[0] // 2 + self.gap_camera:  # right
            self.camera.move_right(x - SCREEN_SIZE[0] // 2 - self.gap_camera)
        if y < SCREEN_SIZE[1] // 2 - self.gap_camera:  # up
            self.camera.move_up(SCREEN_SIZE[1] // 2 - self.gap_camera - y)
        elif y > SCREEN_SIZE[1] // 2 + self.gap_camera:  # down
            self.camera.move_down(y - SCREEN_SIZE[1] // 2 - self.gap_camera)

        # update world
        for tab in self.world:
            for item in tab:
                item.update()

        # update player
        self.player.update(events)
        for door in self.doors:
            if door.use_door(events):
                self.level += 1
                self.init_world()
                break

        # hit a spike
        if self.player.dying():
            self.death += 1
            self.init_world()

        if self.loading:
            if self.frames >= 5:
                self.loading = False
                self.frames = 0
            else:
                self.frames += 1

    def display(self):
        # draw world
        for tab in self.world:
            for item in items_in_screen(tab):
                item.display(self.screen)

        self.player.display(self.screen)

        # loading screen
        if self.loading:
            pg.draw.rect(self.screen, BLACK, pg.Rect(0, 0, SCREEN_SIZE[0], SCREEN_SIZE[1]))

        # display gap camera
        # pg.draw.rect(self.screen, RED, pg.Rect(SCREEN_SIZE[0] // 2 - self.gap_camera, SCREEN_SIZE[1] // 2 - self.gap_camera, self.gap_camera * 2, self.gap_camera * 2), 1)
